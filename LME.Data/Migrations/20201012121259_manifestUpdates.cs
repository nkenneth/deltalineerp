﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class manifestUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Commision",
                table: "Manifest",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "DriverFee",
                table: "Manifest",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "MTU",
                table: "Manifest",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Transload",
                table: "Manifest",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "VAT",
                table: "Manifest",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Commision",
                table: "Manifest");

            migrationBuilder.DropColumn(
                name: "DriverFee",
                table: "Manifest");

            migrationBuilder.DropColumn(
                name: "MTU",
                table: "Manifest");

            migrationBuilder.DropColumn(
                name: "Transload",
                table: "Manifest");

            migrationBuilder.DropColumn(
                name: "VAT",
                table: "Manifest");
        }
    }
}
