﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class removepurchaseDate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PurchaseDate",
                table: "Vehicle");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "PurchaseDate",
                table: "Vehicle",
                nullable: true);
        }
    }
}
