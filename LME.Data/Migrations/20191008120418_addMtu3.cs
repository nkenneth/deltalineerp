﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class addMtu3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "MtuReportModel",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "MtuReportModel",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "MtuReportModel");

            migrationBuilder.DropColumn(
                name: "FullName",
                table: "MtuReportModel");
        }
    }
}
