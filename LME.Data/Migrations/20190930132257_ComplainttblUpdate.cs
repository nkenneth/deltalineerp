﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class ComplainttblUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Complaint",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "Complaint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "Complaint",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Complaint",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Complaint",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Complaint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "Complaint",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TransDate",
                table: "Complaint",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Complaint");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Complaint");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "Complaint");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Complaint");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Complaint");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Complaint");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "Complaint");

            migrationBuilder.DropColumn(
                name: "TransDate",
                table: "Complaint");
        }
    }
}
