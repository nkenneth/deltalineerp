﻿using LME.Core.Entities.Common;
using LME.Core.Entities.Enums;
using System;

namespace LME.Core.Entities
{
    public class Coupon : AuditedEntity<Guid>
    {
        public string CouponCode { get; set; }
        public decimal CouponValue { get; set; }
        public CouponType CouponType { get; set; }

        public bool Validity { get; set; }
        public DurationType DurationType { get; set; }
        public int Duration { get; set; }
    }
}