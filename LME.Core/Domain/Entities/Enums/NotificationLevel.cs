﻿namespace LME.Core.Entities.Enums
{
    public enum NotificationLevel
    {
        None,
        FirstAlert,
        SecondAlert
    }
}