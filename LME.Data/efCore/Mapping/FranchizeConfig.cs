﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class FranchizeConfig : IEntityTypeConfiguration<Franchize>
    {
        public void Configure(EntityTypeBuilder<Franchize> builder)
        {
            builder.ToTable(nameof(Franchize));
        }
    }
}