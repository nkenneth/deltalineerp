﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class FranchiseUserConfig : IEntityTypeConfiguration<FranchiseUser>
    {
        public void Configure(EntityTypeBuilder<FranchiseUser> builder)
        {
            builder.ToTable(nameof(FranchiseUser));

        }
    }
}