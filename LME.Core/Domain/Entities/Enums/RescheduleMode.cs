﻿namespace LME.Core.Entities.Enums
{
    public enum RescheduleMode
    {
        Admin,
        Android,
        IOS,
        OnlineWebsite,
        OnlineMobile
    }
}