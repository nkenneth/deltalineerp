﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class SP_GetChannelsBookings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[Sp_Bookings] (@pageIndex INT, @pageSize INT, @sDate NVARCHAR(20),
		@eDate NVARCHAR(20), @status int, @keyword NVARCHAR(200), @channel int, @terminalId int) 
AS 
  
  BEGIN 
  SET DATEFORMAT dmy;
   
    DECLARE @startDate DATE=NULL, @endDate DATE=NULL; 

    IF(Isdate(@sDate)=1) 
    SET @startDate=Cast (@sDate AS DATE); 
	print @startDate

    IF(Isdate(@eDate)=1) 
    SET @endDate=Cast (@eDate AS DATE); 
    
	SELECT sm.Id, sm.SeatNumber, sm.BookingReferenceCode, sm.PhoneNumber as CustomerPhoneNumber,
								sm.NextOfKinName, sm.Amount, sm.Discount, sm.BookingStatus, sm.BookingType,
								(sm.Amount - sm.Discount) as DiscountedAmount, 
								 FORMAT(sm.CreationTime, 'MMM dd, yyyy') BookingDate,
								--ISNULL(usr.FirstName, '') + ' ' + ISNULL(usr.LastName, '') [EmployeeName], usr.Email as CreatorEmail
								depterm.[Name] as TerminalName, destTerm.[Name] as Destination, 
								[TotalSales] = Sum(sm.Amount) over(),
								[TotalDiscountedSales] = sum(sm.Amount - sm.Discount) over (),
								count(*) over() TotalCount  
    FROM     seatmanagement sm
			 join AspNetUsers usr
	                        on sm.CreatedBy = usr.UserName
	                        left join Employee emp 
	                        on emp.UserId = usr.Id
							join [Route] rt 
							on rt.Id = sm.RouteId
	                        join Terminal depterm 
	                        on depterm.Id = rt.DepartureTerminalId
							join Terminal destTerm
							on destTerm.Id = rt.DestinationTerminalId

    WHERE    (@channel IS NULL OR @channel = sm.bookingtype) 
    AND      (@status IS NULL OR  @status =sm.BookingStatus)
	AND      (@terminalId IS NULL OR  @terminalId = depterm.id)
	AND      ((@Keyword IS NULL) 
			  OR (sm.couponcode LIKE '%' + @Keyword + '%') 
              OR (sm.fullname LIKE '%' + @Keyword + '%') 
              OR (sm.mainbookerreferencecode LIKE '%' + @Keyword + '%') 
              OR (sm.bookingreferencecode LIKE '%' + @Keyword + '%') 
              OR (sm.phonenumber LIKE '%' + @Keyword + '%') 
              OR (sm.nextofkinphonenumber LIKE '%' + @Keyword + '%') 
              OR (sm.onlinesubroutename LIKE '%' + @Keyword + '%')) 
    AND      sm.isdeleted=0 
    AND      ((@startDate IS NULL OR Cast (sm.creationtime AS DATE)>=@startDate )
			 AND
             (@endDate IS NULL OR CONVERT(DATE,sm.creationtime)<=@endDate))

    ORDER BY sm.creationtime DESC offset (@pageSize * (@PageIndex-1)) rows 
    FETCH next @pageSize rows only 
  END");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
