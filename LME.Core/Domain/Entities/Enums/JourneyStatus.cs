﻿namespace LME.Core.Entities.Enums
{
    public enum JourneyStatus
    {
        Pending,
        Approved,
        InTransit,
        Received,
        Transloaded,
        Denied,
    }
}