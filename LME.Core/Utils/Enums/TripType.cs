﻿namespace LME.Core.Common.Enums
{
    public enum TripType
    {
        OneWay,
        Return
    }
}