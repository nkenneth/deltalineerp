﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class MtuPhoto3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MtuPhotoId",
                table: "MtuReportModel");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "MtuPhoto",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "MtuPhoto",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "MtuPhoto",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "MtuPhoto",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "MtuPhoto",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "MtuPhoto",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "MtuPhoto",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "MtuPhoto");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "MtuPhoto");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "MtuPhoto");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "MtuPhoto");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "MtuPhoto");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "MtuPhoto");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "MtuPhoto");

            migrationBuilder.AddColumn<int>(
                name: "MtuPhotoId",
                table: "MtuReportModel",
                nullable: false,
                defaultValue: 0);
        }
    }
}
