﻿namespace LME.Core.Entities.Enums
{
    public enum PickupStatus
    {
        NotForPickUp,
        PendingPickup,
        Pickedup,
        NoShow
    }
}