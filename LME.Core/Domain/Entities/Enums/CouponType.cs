﻿namespace LME.Core.Entities.Enums
{
    public enum CouponType
    {
        Percentage,
        Fixed
    }
}