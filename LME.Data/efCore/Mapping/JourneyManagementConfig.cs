﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace LME.Data.efCore.Mapping
{
    class JourneyManagementConfig : IEntityTypeConfiguration<JourneyManagement>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<JourneyManagement> builder)
        {
            builder.HasKey(x => x.Id);
            builder.ToTable(nameof(JourneyManagement));

        }
    }
}