﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class Sp_AdvancedBookingSummary : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[Sp_AdvancedBookingSalesSummary] (@pageIndex INT, @pageSize INT, @sDate NVARCHAR(20),
		@eDate NVARCHAR(20),  @employeeId int, @terminalId int) 
AS 
  
  BEGIN 
  SET DATEFORMAT dmy;
   
    DECLARE @startDate DATE=GetDate(), @endDate DATE=GetDate(); 

    IF(Isdate(@sDate)=1) 
    SET @startDate=Cast (@sDate AS DATE); 
	
    IF(Isdate(@eDate)=1) 
    SET @endDate= Cast (@eDate AS DATE); 
    
	SELECT sm.PaymentMethod, FORMAT(sm.CreationTime, 'MMM dd, yyyy') BookingDate,
	FORMAT(sm.LastModificationTime, 'MMM dd, yyyy') DepartureDate,
	v.RegistrationNumber VehicleNumber,
	
	isnull(case sm.PaymentMethod 
	when 0 then sum(sm.Amount) over(partition by sm.PaymentMethod)
	end,0) CashSales,

	Isnull(case sm.PaymentMethod 
	when 1 then sum(sm.Amount) over(partition by sm.PaymentMethod)
	end,0) PosSales,
	sum(sm.Amount) over() TotalSales,
	count(*) over() TotalCount,
	(sm.Amount - sm.Discount) DiscountedAmount, sm.Amount, sm.BookingReferenceCode,
	ISNULL(usr.FirstName, '') + ' ' + ISNULL(usr.LastName, '') [Employee]
    
	FROM     seatmanagement sm
	         left join AspNetUsers usr on sm.CreatedBy = usr.UserName
	         left join Employee emp  on emp.UserId = usr.Id
			 join [Route] rt on rt.Id = sm.RouteId
			 join Terminal depterm on depterm.Id = rt.DepartureTerminalId
			 left join vehicletripregistration vtr on vtr.id=sm.vehicletripregistrationId
			 left Join vehicle v on v.RegistrationNumber = vtr.PhysicalBusRegistrationNumber
			 
    WHERE    (sm.bookingtype =1 ) 
    AND      (@employeeId IS NULL OR  @employeeId = emp.Id)
	AND      (@terminalId IS NULL OR  @terminalId = depterm.id)
	
    AND      sm.isdeleted=0 
    AND      ((@startDate IS NULL OR Cast (sm.creationtime AS DATE)>= @startDate )
			 AND
             (@endDate IS NULL OR CONVERT(DATE,sm.creationtime)<= @endDate))
  END");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
