﻿namespace LME.Core.Entities.Enums
{
    public enum SourceType
    {
        Inventory,
        Workshop,
        Account
    }
}