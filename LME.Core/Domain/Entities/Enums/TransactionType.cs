﻿namespace LME.Core.Entities.Enums
{
    public enum TransactionType
    {
        Debit,
        Credit
    }
}
