﻿using LME.Core.Entities.Common;

namespace LME.Core.Entities
{
    public class VehicleMake : FullAuditedEntity
    {
        public string Name { get; set; }
    }
}