﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class rolecreationdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Otp",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "OtpIsUsed",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "OtpNoOfTimeUsed",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "TicketRemovalOtpIsUsed",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "EmployeeCode",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "TicketRemovalOtp",
                table: "Employee",
                newName: "EmployeeCode");

            migrationBuilder.RenameColumn(
                name: "OTPLastUsedDate",
                table: "Employee",
                newName: "DateOfEmployment");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "AspNetRoles",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "AspNetRoles");

            migrationBuilder.RenameColumn(
                name: "EmployeeCode",
                table: "Employee",
                newName: "TicketRemovalOtp");

            migrationBuilder.RenameColumn(
                name: "DateOfEmployment",
                table: "Employee",
                newName: "OTPLastUsedDate");

            migrationBuilder.AddColumn<string>(
                name: "Otp",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "OtpIsUsed",
                table: "Employee",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "OtpNoOfTimeUsed",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "TicketRemovalOtpIsUsed",
                table: "Employee",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "EmployeeCode",
                table: "AspNetUsers",
                nullable: true);
        }
    }
}
