﻿namespace LME.Core.Entities.Enums
{
    public enum PaymentStatus
    {
        Paid,
        Pendding,
        Declined
    }
}