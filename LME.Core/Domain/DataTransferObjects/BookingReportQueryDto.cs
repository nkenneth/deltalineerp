﻿using LME.Core.Entities.Enums;
using System;

namespace LME.Core.Domain.DataTransferObjects
{
    public class BookingReportQueryDto
    {
        public int? TerminalId { get; set; }
        public int? BookingType { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string Keyword { get; set; }
        public string CreatedBy { get; set; }
        public string ReferenceCode { get; set; }
        public int? BookingStatus { get; set; }

        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }

    public class BookingReportQueryViewModel
    {
        public int? TerminalId { get; set; }
        public int? ChannelId { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public string Keyword { get; set; }
        public int? Status { get; set; }

        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }

    public class AdvancedBookingReportQueryViewModel
    {
        public int? TerminalId { get; set; }
        public int? EmployeeId { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }

    public class ChannelReportQueryViewModel
    {
        public int? TerminalId { get; set; }
        public int? ChannelId { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public int? Status { get; set; }
    }

    public class ChannelBookingSalesSummaryDto
    {
        public BookingTypes BookingType { get; set; }
        public decimal SalesAmmount { get; set; }
        public decimal DiscountedAmount { get; set; }
    }

    public class ChannelBookingSalesSummaryViewModel
    {
        public string BookingType { get; set; }
        public decimal SalesAmmount { get; set; }
        public decimal DiscountedAmount { get; set; }


        public static implicit operator ChannelBookingSalesSummaryViewModel(ChannelBookingSalesSummaryDto source)
        {
            var destination = new ChannelBookingSalesSummaryViewModel
            {
                BookingType = source.BookingType.ToString(),
                SalesAmmount = source.SalesAmmount,
                DiscountedAmount = source.DiscountedAmount
            };
            return destination;
        }
    }
}