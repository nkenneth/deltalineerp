﻿using LME.Business.Services;
using LME.Core.Domain.DataTransferObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace LME.WebAPI.Controllers
{
    [ApiController]
    [Authorize]
    public class ReportController : BaseController
    {
        private readonly IBookingReportService _bookingReportService;
        private readonly IServiceHelper _serviceHelper;

        public ReportController(IBookingReportService bookingReportService, IServiceHelper serviceHelper)
        {
            _bookingReportService = bookingReportService;
            _serviceHelper = serviceHelper;
        }

        [HttpPost]
        [Route("search")]
        public async Task<IServiceResponse<List<BookingReportDto>>> BookingReports(BookingReportQueryDto queryDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.GetBookingReport(queryDto);
                return new ServiceResponse<List<BookingReportDto>>
                {
                    Object = reports
                };
            });
        }

        [HttpPost]
        [Route("bookedtrips")]
        public async Task<IServiceResponse<List<BookedTripsDto>>> AllBookedTrips(BookedTripsQueryDto queryDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.BookedTrips(queryDto);
                return new ServiceResponse<List<BookedTripsDto>>
                {
                    Object = reports.OrderByDescending(p => p.DepartureDate).ToList()
                };
            });
        }

        [HttpPost]
        [Route("bookedsales")]
        public async Task<IServiceResponse<List<BookingSalesReport>>> BookingSales(BookingSalesReportQueryDto queryDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.BookingSalesReport(queryDto);
                return new ServiceResponse<List<BookingSalesReport>>
                {
                    Object = reports.OrderByDescending(p => p.Amount).ToList()
                };
            });
        }

        [HttpPost]
        [Route("passengerReport")]
        public async Task<IServiceResponse<List<PassengerReportDto>>> PassengerReport(PassengerReportQueryDto queryDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.PassengerReport(queryDto);
                return new ServiceResponse<List<PassengerReportDto>>
                {
                    Object = reports.ToList()
                };
            });
        }

        [HttpPost]
        [Route("salesbybus")]
        public async Task<IServiceResponse<List<SalesPerBusDTO>>> SalesByBus(SalesByBusQueryDto queryDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.SalesByBus(queryDto);
                return new ServiceResponse<List<SalesPerBusDTO>>
                {
                    Object = reports
                };
            });
        }

        [HttpPost]
        [Route("driversalary")]
        public async Task<IServiceResponse<List<DriverSalaryReportModel>>> DriverSalary(SalaryReportQuery queryDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.DriverSalaryReport(queryDto);
                return new ServiceResponse<List<DriverSalaryReportModel>>
                {
                    Object = reports
                };
            });
        }

        [HttpPost]
        [Route("hiretrips")]

        public async Task<IServiceResponse<List<HireRequestDTO>>> HiredTrips(HiredTripRequestDTO requestDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.HiredTripReportAsync(requestDto);
                return new ServiceResponse<List<HireRequestDTO>>
                {
                    Object = reports
                };
            });
        }


        [HttpPost]
        [Route("journeyCharts")]

        public async Task<IServiceResponse<List<JourneyChartDto>>> JourneyCharts(JourneyChartQueryDto requestDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.JourneyChartReport(requestDto);
                return new ServiceResponse<List<JourneyChartDto>>
                {
                    Object = reports
                };
            });
        }

        [HttpPost]
        [Route("journeyChartsForStateTerminals")]

        public async Task<IServiceResponse<List<JourneyChartDto>>> JourneyChartsForStateTerminals(JourneyChartQueryDto requestDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.JourneyChartForStateTerminals(requestDto);
                return new ServiceResponse<List<JourneyChartDto>>
                {
                    Object = reports
                };
            });
        }

        [HttpPost]
        [Route("journeyChartsForTerminals")]

        public async Task<IServiceResponse<List<JourneyDetailDisplayDTO>>> JourneyChartsForTerminal(JourneyChartQueryDto requestDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.JourneyChartForTerminals(requestDto);
                return new ServiceResponse<List<JourneyDetailDisplayDTO>>
                {
                    Object = reports
                };
            });
        }

        [HttpPost]
        [Route("totalSalesReportPerState")]

        public async Task<IServiceResponse<List<SalesReportDTO>>> TotalSalesReportByState(DateModel requestDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.TotalSalesPerState(requestDto);
                return new ServiceResponse<List<SalesReportDTO>>
                {
                    Object = reports
                };
            });
        }

        [HttpPost]
        [Route("totalTerminalSalesInState")]

        public async Task<IServiceResponse<List<SalesReportDTO>>> TotalTerminalSalesInState(DateModel requestDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.TotalTerminalSalesInState(requestDto);
                return new ServiceResponse<List<SalesReportDTO>>
                {
                    Object = reports
                };
            });
        }

        [HttpPost]
        [Route("terminalSalesSummary")]

        public async Task<IServiceResponse<List<SalesReportDTO>>> TerminalSalesSummary(DateModel requestDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.TotalTerminalSalesSummary(requestDto);
                return new ServiceResponse<List<SalesReportDTO>>
                {
                    Object = reports
                };
            });
        }

        [HttpGet]
        [Route("getsalessummary")]

        public async Task<IServiceResponse<SalesSummaryDTO>> SalesSummary()
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.GetSalesSummary();
                return new ServiceResponse<SalesSummaryDTO>
                {
                    Object = reports
                };
            });
        }

        [HttpGet]
        [Route("getbookingsummary")]

        public async Task<IServiceResponse<BookingSummaryDto>> BookingSummary()
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.GetBookingSummary();
                return new ServiceResponse<BookingSummaryDto>
                {
                    Object = reports
                };
            });
        }

        [HttpPost]
        [Route("totalRevenueReportPerState")]

        public async Task<IServiceResponse<List<SalesReportDTO>>> TotalRevenueReportByState(DateModel requestDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.TotalRevenuePerState(requestDto);
                return new ServiceResponse<List<SalesReportDTO>>
                {
                    Object = reports
                };
            });
        }

        [HttpPost]
        [Route("totalTerminalRevenueInState")]

        public async Task<IServiceResponse<List<SalesReportDTO>>> TotalTerminalRevenueInState(DateModel requestDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.TotalTerminalRevenueInState(requestDto);
                return new ServiceResponse<List<SalesReportDTO>>
                {
                    Object = reports
                };
            });
        }

        [HttpPost]
        [Route("terminalRevenueSummary")]

        public async Task<IServiceResponse<List<SalesReportDTO>>> TerminalRevenueSummary(DateModel requestDto)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.TotalTerminalRevenueSummary(requestDto);
                return new ServiceResponse<List<SalesReportDTO>>
                {
                    Object = reports
                };
            });
        }


        [HttpGet]
        [Route("advancedbookings")]
        public async Task<IServiceResponse<AdvancedBookingSalesSummary<AdvancedBookingReportViewModel>>> AdvancedBookings([FromQuery] AdvancedBookingReportQueryViewModel model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.AdvancedBooking(model.PageIndex,
                    model.PageSize, model.EmployeeId,
                    model.StartDate, model.EndDate, model.TerminalId);

                return new ServiceResponse<AdvancedBookingSalesSummary<AdvancedBookingReportViewModel>>
                {
                    Object = reports
                };
            });
        }

        [HttpGet]
        [Route("bookings")]
        public async Task<IServiceResponse<BookingSalesSummary<BookingReportViewModel>>> Bookings([FromQuery] BookingReportQueryViewModel model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.GetBookings(model.PageIndex,
                    model.PageSize,
                    model.ChannelId,
                    model.Status,
                    model.Keyword,
                    model.StartDate, model.EndDate, model.TerminalId);

                return new ServiceResponse<BookingSalesSummary<BookingReportViewModel>>
                {
                    Object = reports
                };
            });
        }

        [HttpGet]
        [Route("channelsalessummary")]
        public async Task<IServiceResponse<IEnumerable<ChannelBookingSalesSummaryViewModel>>> ChannelSalesSummary([FromQuery] ChannelReportQueryViewModel model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var reports = await _bookingReportService.GetChannelSalesSummaryBookings(
                    model.ChannelId,
                    model.Status,
                    model.StartDate, model.EndDate, model.TerminalId);

                return new ServiceResponse<IEnumerable<ChannelBookingSalesSummaryViewModel>>
                {
                    Object = reports
                };
            });
        }

        [HttpGet]
        [Route("ticketertotaldailysales/{email}")]
        public async Task<IServiceResponse<string>> GetTicketerTotalDailySales(string email)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var sales = await _bookingReportService.GetTotalSalesByTicketer(email);

                return new ServiceResponse<string>
                {
                    Object = sales.ToString("N", new CultureInfo("en-US"))
                };
            });
        }

        [HttpGet]
        [Route("ticketertotaldailysales")]
        public async Task<IServiceResponse<string>> GetTicketerTotalDailySales()
        {
            return await HandleApiOperationAsync(async () =>
            {
                var sales = await _bookingReportService.GetTotalSalesByTicketer(_serviceHelper.GetCurrentUserEmail());

                return new ServiceResponse<string>
                {
                    Object = sales.ToString("N", new CultureInfo("en-US"))
                };
            });
        }

        [HttpGet]
        [Route("ticketertotalposdailysales/{email}")]
        public async Task<IServiceResponse<string>> GetTicketerTotalPosSales(string email)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var sales = await _bookingReportService.GetTotalPosByTicketer(email);

                return new ServiceResponse<string>
                {
                    Object = sales.ToString("N", new CultureInfo("en-US"))
                };
            });
        }

        [HttpGet]
        [Route("ticketertotalposdailysales")]
        public async Task<IServiceResponse<string>> GetTicketerTotalPosSales()
        {
            return await HandleApiOperationAsync(async () =>
            {
                var sales = await _bookingReportService.GetTotalPosByTicketer(_serviceHelper.GetCurrentUserEmail());

                return new ServiceResponse<string>
                {
                    Object = sales.ToString("N", new CultureInfo("en-US"))
                };
            });
        }

        [HttpGet]
        [Route("ticketertotalcashdailysales/{email}")]
        public async Task<IServiceResponse<string>> GetTicketerTotalCashSales(string email)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var sales = await _bookingReportService.GetTotalCashByTicketer(email);

                return new ServiceResponse<string>
                {
                    Object = sales.ToString("N", new CultureInfo("en-US"))
                };
            });
        }

        [HttpGet]
        [Route("ticketertotalcashdailysales")]
        public async Task<IServiceResponse<string>> GetTicketerTotalCashSales()
        {
            return await HandleApiOperationAsync(async () =>
            {
                var sales = await _bookingReportService.GetTotalCashByTicketer(_serviceHelper.GetCurrentUserEmail());

                return new ServiceResponse<string>
                {
                    Object = sales.ToString("N", new CultureInfo("en-US"))
                };
            });
        }

        [HttpGet]
        [Route("ticketerdailysalesdetails")]
        public async Task<IServiceResponse<List<DLSalesReportDTO>>> GetTicketerDailySales()
        {
            return await HandleApiOperationAsync(async () =>
            {
                var sales = await _bookingReportService.GetTicketerDailySales(_serviceHelper.GetCurrentUserEmail());

                return new ServiceResponse<List<DLSalesReportDTO>>(sales);
            });
        }

        [HttpGet]
        [Route("ticketerdailysalesdetails/{email}")]
        public async Task<IServiceResponse<List<DLSalesReportDTO>>> GetTicketerDailySales(string email)
        {
          
            return await HandleApiOperationAsync(async () =>
            {
                var sales = await _bookingReportService.GetTicketerDailySales(email);

                return new ServiceResponse<List<DLSalesReportDTO>>(sales);
            });
        }

        [HttpGet]
        [Route("ticketerdailypossalesdetails")]
        public async Task<IServiceResponse<List<DLSalesReportDTO>>> GetTicketerDailyPosSales()
        {
            return await HandleApiOperationAsync(async () =>
            {
                var sales = await _bookingReportService.GetTicketerDailyPosSales(_serviceHelper.GetCurrentUserEmail());

                return new ServiceResponse<List<DLSalesReportDTO>>
                {
                    Object = sales
                };
            });
        }

        [HttpGet]
        [Route("ticketerdailypossalesdetails/{email}")]
        public async Task<IServiceResponse<List<DLSalesReportDTO>>> GetTicketerDailyPosSales(string email)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var sales = await _bookingReportService.GetTicketerDailyPosSales(email);

                return new ServiceResponse<List<DLSalesReportDTO>>
                {
                    Object = sales
                };
            });
        }

        [HttpGet]
        [Route("ticketerdailycashsalesdetails")]
        public async Task<IServiceResponse<List<DLSalesReportDTO>>> GetTicketerDailyCashSales()
        {
            return await HandleApiOperationAsync(async () =>
            {
                var sales = await _bookingReportService.GetTicketerDailyCashSales(_serviceHelper.GetCurrentUserEmail());

                return new ServiceResponse<List<DLSalesReportDTO>>
                {
                    Object = sales
                };
            });
        }

        [HttpGet]
        [Route("ticketerdailycashsalesdetails/{email}")]
        public async Task<IServiceResponse<List<DLSalesReportDTO>>> GetTicketerDailyCashSales(string email)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var sales = await _bookingReportService.GetTicketerDailyCashSales(email);

                return new ServiceResponse<List<DLSalesReportDTO>>
                {
                    Object = sales
                };
            });
        }
    }
}