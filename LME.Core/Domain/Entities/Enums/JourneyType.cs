﻿namespace LME.Core.Entities.Enums
{
    public enum JourneyType
    {
        Loaded,
        Blown,
        Pickup,
        Rescue,
        Transload
    }
}