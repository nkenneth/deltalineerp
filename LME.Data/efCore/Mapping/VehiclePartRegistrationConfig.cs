﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class VehiclePartRegistrationConfig : IEntityTypeConfiguration<VehiclePartRegistration>
    {
        public void Configure(EntityTypeBuilder<VehiclePartRegistration> builder)
        {
            builder.ToTable(nameof(VehiclePartRegistration));

        }
    }
}
