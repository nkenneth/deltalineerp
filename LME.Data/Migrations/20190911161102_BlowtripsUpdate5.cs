﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class BlowtripsUpdate5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DriverId",
                table: "BlowTrip",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "BlowTrip",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VehicleId",
                table: "BlowTrip",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DriverId",
                table: "BlowTrip");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "BlowTrip");

            migrationBuilder.DropColumn(
                name: "VehicleId",
                table: "BlowTrip");
        }
    }
}
