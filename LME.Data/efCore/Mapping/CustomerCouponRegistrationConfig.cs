﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class CustomerCouponRegistrationConfig : IEntityTypeConfiguration<CustomerCouponRegistration>
    {
        public void Configure(EntityTypeBuilder<CustomerCouponRegistration> builder)
        {
            builder.HasKey(x => x.Id);
            builder.ToTable(nameof(CustomerCouponRegistration));
        }
    }
}