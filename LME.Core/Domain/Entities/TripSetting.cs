﻿using LME.Core.Entities.Common;
using LME.Core.Entities.Enums;
using System;
using System.Collections.Generic;

namespace LME.Core.Entities
{
    public class TripSetting : FullAuditedEntity<Guid>
    {
        public Guid TripSettingId { get; set; }
        public int RouteId { get; set; }
        public WeekDays WeekDays { get; set; }


        public virtual ICollection<TripAvailability> AvailableTrips { get; set; }

    }
}
