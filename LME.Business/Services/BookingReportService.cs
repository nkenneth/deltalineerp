﻿using IPagedList;
using LME.Core.Domain.DataTransferObjects;
using LME.Core.Entities.Enums;
using LME.Core.Timing;
using LME.Data.efCore.Context;
using LME.Data.UnitOfWork;
using LME.Data.Utils.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LME.Business.Services
{
    public interface IBookingReportService
    {
        Task<List<BookingReportDto>> GetBookingReport(BookingReportQueryDto queryDto);
        Task<List<BookedTripsDto>> BookedTrips(BookedTripsQueryDto queryDto);
        Task<List<BookingSalesReport>> BookingSalesReport(BookingSalesReportQueryDto queryDto);
        Task<List<PassengerReportDto>> PassengerReport(PassengerReportQueryDto queryDto);
        Task<List<SalesPerBusDTO>> SalesByBus(SalesByBusQueryDto queryDto);
        Task<List<DriverSalaryReportModel>> DriverSalaryReport(SalaryReportQuery queryDto);
        Task<List<HireRequestDTO>> HiredTripReportAsync(HiredTripRequestDTO queryDto);
        Task<List<JourneyChartDto>> JourneyChartReport(JourneyChartQueryDto queryDto);
        Task<List<JourneyChartDto>> JourneyChartForStateTerminals(JourneyChartQueryDto queryDto);
        Task<SalesSummaryDTO> GetSalesSummary();
        Task<BookingSummaryDto> GetBookingSummary();
        Task<List<JourneyDetailDisplayDTO>> JourneyChartForTerminals(JourneyChartQueryDto details);
        Task<List<SalesReportDTO>> TotalSalesPerState(DateModel date);
        Task<List<SalesReportDTO>> TotalTerminalSalesInState(DateModel date);
        Task<List<SalesReportDTO>> TotalTerminalSalesSummary(DateModel date);
        Task<List<SalesReportDTO>> TotalRevenuePerState(DateModel date);
        Task<List<SalesReportDTO>> TotalTerminalRevenueInState(DateModel date);
        Task<List<SalesReportDTO>> TotalTerminalRevenueSummary(DateModel date);
        Task<List<DLSalesReportDTO>> GetSalesReportByDateAsync(SalesReportQueryDTO queryDto);

        Task<BookingSalesSummary<BookingReportViewModel>> GetBookings(
           int pageNumber,
           int pageSize,
           int? channel,
           int? status,
           string keyword,
           string startDate,
           string endDate,
           int? terminalId
           );

        Task<IEnumerable<ChannelBookingSalesSummaryViewModel>> GetChannelSalesSummaryBookings(
            int? channel,
            int? status,
            string startDate,
            string endDate,
            int? terminalId
            );

        Task<AdvancedBookingSalesSummary<AdvancedBookingReportViewModel>> AdvancedBooking(
            int pageNumber,
            int pageSize,
            int? employeeId,
            string startDate,
            string endDate,
            int? terminalId
            );
        Task<decimal> GetTotalSalesByTicketer(string username);
        Task<decimal> GetTotalPosByTicketer(string username);
        Task<decimal> GetTotalCashByTicketer(string username);
        Task<List<DLSalesReportDTO>> GetTicketerDailySales(string email);
        Task<List<DLSalesReportDTO>> GetTicketerDailyPosSales(string email);
        Task<List<DLSalesReportDTO>> GetTicketerDailyCashSales(string email);
    }

    public class BookingReportService : IBookingReportService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISeatManagementService _seatManagementService;
        private readonly IVehicleTripRegistrationService _vehicleTripRegService;


        public BookingReportService(IUnitOfWork unitOfWork, ISeatManagementService seatManagementService,
            IVehicleTripRegistrationService vehicleTripRegService)
        {
            _unitOfWork = unitOfWork;
            _seatManagementService = seatManagementService;
            _vehicleTripRegService = vehicleTripRegService;
        }
        public async Task<List<BookingReportDto>> GetBookingReport(BookingReportQueryDto queryDto)
        {
            if (queryDto.StartDate == null)
            {
                queryDto.StartDate = Clock.Now.Date;
            }
            if (queryDto.EndDate == null)
            {
                queryDto.EndDate = Clock.Now;
            }

            var reports = await _unitOfWork
                .GetDbContext<ApplicationDbContext>()
                .Database.ExecuteSqlToObject<BookingReportDto>(@"Exec Sp_BookingReport",
                                                                queryDto.TerminalId, queryDto.Keyword, queryDto.BookingType, queryDto.BookingStatus, queryDto.CreatedBy, queryDto.ReferenceCode,
                                                                queryDto.StartDate, queryDto.EndDate, queryDto.PageIndex, queryDto.PageSize);

            return reports.ToList();
        }
        public async Task<List<BookedTripsDto>> BookedTrips(BookedTripsQueryDto queryDto)
        {
            if (queryDto.StartDate == null)
            {
                queryDto.StartDate = Clock.Now.Date;
            }
            if (queryDto.EndDate == null)
            {
                queryDto.EndDate = Clock.Now;
            }
            queryDto.PhysicalBusRegisterationNumber = string.IsNullOrEmpty(queryDto.PhysicalBusRegisterationNumber) ? null : queryDto.PhysicalBusRegisterationNumber;
            var reports = await _unitOfWork
                .GetDbContext<ApplicationDbContext>()
                .Database.ExecuteSqlToObject<BookedTripsDto>(@"Exec Sp_BookedBusesReports", queryDto.BookingType,
                                                             queryDto.PhysicalBusRegisterationNumber, queryDto.StartDate, queryDto.EndDate);

            return reports.ToList();
        }

        public async Task<List<BookingSalesReport>> BookingSalesReport(BookingSalesReportQueryDto queryDto)
        {
            if (queryDto.StartDate == null)
            {
                queryDto.StartDate = Clock.Now.Date;
            }
            if (queryDto.EndDate == null)
            {
                queryDto.EndDate = Clock.Now;
            }
            queryDto.TerminalId = queryDto.TerminalId == 0 ? null : queryDto.TerminalId;
            queryDto.PaymentMethod = queryDto.PaymentMethod == 0 ? null : queryDto.PaymentMethod;
            queryDto.StateId = queryDto.StateId == 0 ? null : queryDto.StateId;
            queryDto.RouteId = queryDto.RouteId == 0 ? null : queryDto.RouteId;

            var reports = await _unitOfWork
                .GetDbContext<ApplicationDbContext>()
                .Database.ExecuteSqlToObject<BookingSalesReport>(@"Exec Sp_Salesreport",
                queryDto.RouteId, queryDto.TerminalId, queryDto.StateId, queryDto.PaymentMethod, queryDto.CreatedBy, queryDto.StartDate, queryDto.EndDate);

            return reports.ToList();
        }

        public async Task<List<PassengerReportDto>> PassengerReport(PassengerReportQueryDto queryDto)
        {
            if (queryDto.StartDate == null)
            {
                queryDto.StartDate = Clock.Now.Date;
            }
            if (queryDto.EndDate == null)
            {
                queryDto.EndDate = Clock.Now;
            }

            var reports = await _unitOfWork
                .GetDbContext<ApplicationDbContext>()
                .Database.ExecuteSqlToObject<PassengerReportDto>(@"Exec Sp_PassengerReport",
                queryDto.Keyword, queryDto.StartDate, queryDto.EndDate);

            return reports.ToList();
        }
        public async Task<List<SalesPerBusDTO>> SalesByBus(SalesByBusQueryDto queryDto)
        {
            if (queryDto.StartDate == null)
            {
                queryDto.StartDate = Clock.Now.Date;
            }
            if (queryDto.EndDate == null)
            {
                queryDto.EndDate = Clock.Now;
            }

            var reports = await _unitOfWork
                .GetDbContext<ApplicationDbContext>()
                .Database.ExecuteSqlToObject<SalesPerBusDTO>(@"Exec Sp_SalesByBus",
               queryDto.StartDate, queryDto.EndDate);

            return reports.ToList();
        }
        public async Task<List<DriverSalaryReportModel>> DriverSalaryReport(SalaryReportQuery queryDto)
        {
            if (queryDto.StartDate == null)
            {
                queryDto.StartDate = Clock.Now.Date;
            }
            if (queryDto.EndDate == null)
            {
                queryDto.EndDate = Clock.Now;
            }

            var reports = await _unitOfWork
                .GetDbContext<ApplicationDbContext>()
                .Database.ExecuteSqlToObject<DriverSalaryReportModel>(@"Exec Sp_DriverSalary", queryDto.DriverCode,
               queryDto.StartDate, queryDto.EndDate);

            return reports.ToList();
        }
        public async Task<List<HireRequestDTO>> HiredTripReportAsync(HiredTripRequestDTO queryDto)
        {
            if (queryDto.StartDate == null)
            {
                queryDto.StartDate = Clock.Now.Date;
            }
            if (queryDto.EndDate == null)
            {
                queryDto.EndDate = Clock.Now;
            }

            var reports = await _unitOfWork
             .GetDbContext<ApplicationDbContext>()
             .Database.ExecuteSqlToObject<HireRequestDTO>(@"Exec Sp_HiredRequests", queryDto.Keyword, queryDto.StartDate, queryDto.EndDate
                , queryDto.PageIndex, queryDto.PageSize
             );

            return reports.ToList();

        }
        public async Task<List<JourneyChartDto>> JourneyChartReport(JourneyChartQueryDto queryDto)
        {
            if (queryDto.StartDate == null)
            {
                queryDto.StartDate = Clock.Now.Date;
            }
            if (queryDto.EndDate == null)
            {
                queryDto.EndDate = Clock.Now;

            }

            var reports = await _unitOfWork
             .GetDbContext<ApplicationDbContext>()
             .Database.ExecuteSqlToObject<JourneyChartDto>(@"Exec Sp_JourneyCharts", queryDto.StartDate, queryDto.EndDate
             );

            return reports.ToList();
        }

        public async Task<List<JourneyChartDto>> JourneyChartForStateTerminals(JourneyChartQueryDto queryDto)
        {
            if (queryDto.StartDate == null)
            {
                queryDto.StartDate = Clock.Now.Date;
            }
            if (queryDto.EndDate == null)
            {
                queryDto.EndDate = Clock.Now;

            }

            var reports = await _unitOfWork
             .GetDbContext<ApplicationDbContext>()
             .Database.ExecuteSqlToObject<JourneyChartDto>(@"Exec Sp_JourneyChartsByState", queryDto.StartDate, queryDto.EndDate, queryDto.StateId
             );

            return reports.ToList();
        }

        public async Task<List<JourneyDetailDisplayDTO>> JourneyChartForTerminals(JourneyChartQueryDto details)
        {
            var reports = await _unitOfWork
             .GetDbContext<ApplicationDbContext>()
             .Database.ExecuteSqlToObject<JourneyDetailDisplayDTO>(@"Exec Sp_JourneyChartsByTerminalDetails", details.StartDate,
             details.EndDate, details.DepartureTerminalId, details.DestinationTerminalId,
             details.JourneyStatus, details.JourneyType);

            return reports.ToList();
        }

        public async Task<List<SalesReportDTO>> TotalSalesPerState(DateModel date)
        {
            var reports = await _unitOfWork
             .GetDbContext<ApplicationDbContext>()
             .Database.ExecuteSqlToObject<SalesReportDTO>(@"Exec sp_SalesInStates", date.StartDate,
             date.EndDate, date.Id);

            return reports.ToList();
        }


        public Task<SalesSummaryDTO> GetSalesSummary()
        {
            var today = DateTime.Now.Date;
            var currentDate = DateTime.Now;

            var todaysSales = _seatManagementService.GetAll().Where(p => p.BookingStatus == Core.Entities.Enums.BookingStatus.Approved
                    && (p.CreationTime > today && p.CreationTime < currentDate))?.Sum(p => p.Amount);
            var yesterdaysSales = _seatManagementService.GetAll().Where(p => p.BookingStatus == Core.Entities.Enums.BookingStatus.Approved
                    && (p.CreationTime > today.AddDays(-1) && p.CreationTime < currentDate.AddDays(-1)))?.Sum(p => p.Amount);
            var todaysBookings = _seatManagementService.GetAll().Where(p => p.BookingStatus == Core.Entities.Enums.BookingStatus.Approved
                     && (p.CreationTime > today && p.CreationTime < currentDate))?.Count();
            var summary = new SalesSummaryDTO
            {
                TodaysSales = todaysSales,
                LastSales = yesterdaysSales,
                TodaysBookings = todaysBookings
            };
            return Task.FromResult(summary);
        }
        public Task<BookingSummaryDto> GetBookingSummary()
        {
            var today = DateTime.Now.Date;
            var currentDate = DateTime.Now;

            var onlineBookings = _seatManagementService.GetAll().Where(p => p.BookingType == Core.Entities.Enums.BookingTypes.Online
                    && (p.CreationTime > today && p.CreationTime < currentDate))?.Count();
            var terminalBookings = _seatManagementService.GetAll().Where(p => p.BookingType == Core.Entities.Enums.BookingTypes.Terminal
                    && (p.CreationTime > today && p.CreationTime < currentDate))?.Count();
            var advancedBooking = _seatManagementService.GetAll().Where(p => p.BookingType == Core.Entities.Enums.BookingTypes.Advanced
                 && (p.CreationTime > today && p.CreationTime < currentDate))?.Count();

            var summary = new BookingSummaryDto
            {
                OnlineChannelCount = onlineBookings,
                TerminalBookingCount = terminalBookings,
                AdvancedBookingCount = advancedBooking
            };
            return Task.FromResult(summary);
        }

        public async Task<List<SalesReportDTO>> TotalTerminalSalesInState(DateModel date)
        {
            var reports = await _unitOfWork
             .GetDbContext<ApplicationDbContext>()
             .Database.ExecuteSqlToObject<SalesReportDTO>(@"Exec sp_SalesByTerminalInState", date.StartDate, date.EndDate, date.Id);

            return reports.ToList();
        }

        public async Task<List<SalesReportDTO>> TotalTerminalSalesSummary(DateModel date)
        {
            var reports = await _unitOfWork
             .GetDbContext<ApplicationDbContext>()
             .Database.ExecuteSqlToObject<SalesReportDTO>(@"Exec sp_SalesInTerminal", date.StartDate, date.EndDate, date.BookingType, date.Id);

            return reports.ToList();
        }

        public async Task<List<SalesReportDTO>> TotalRevenuePerState(DateModel date)
        {
            var reports = await _unitOfWork
             .GetDbContext<ApplicationDbContext>()
             .Database.ExecuteSqlToObject<SalesReportDTO>(@"Exec sp_RevenueInStates", date.StartDate,
             date.EndDate, date.Id);

            return reports.ToList();
        }

        public async Task<List<SalesReportDTO>> TotalTerminalRevenueInState(DateModel date)
        {
            var reports = await _unitOfWork
            .GetDbContext<ApplicationDbContext>()
            .Database.ExecuteSqlToObject<SalesReportDTO>(@"Exec sp_RevenueByTerminalInState", date.StartDate, date.EndDate, date.Id);

            return reports.ToList();
        }

        public async Task<List<SalesReportDTO>> TotalTerminalRevenueSummary(DateModel date)
        {
            var reports = await _unitOfWork
            .GetDbContext<ApplicationDbContext>()
            .Database.ExecuteSqlToObject<SalesReportDTO>(@"Exec sp_RevenueInTerminal", date.StartDate, date.EndDate, date.BookingType, date.Id);

            return reports.ToList();
        }



        public async Task<BookingSalesSummary<BookingReportViewModel>> GetBookings(
            int pageNumber,
            int pageSize,
            int? channel,
            int? status,
            string keyword,
            string startDate,
            string endDate,
            int? terminalId
            )
        {
            var dbResults = await _unitOfWork
             .GetDbContext<ApplicationDbContext>()
             .Database.ExecuteSqlToObject<BookingReportDto>(@"Exec Sp_bookings",
             pageNumber, pageSize, startDate, endDate, status, keyword, channel, terminalId);

            var first = dbResults.FirstOrDefault() ?? new BookingReportDto();

            var reports = dbResults.Select(u => (BookingReportViewModel)u);

            return new BookingSalesSummary<BookingReportViewModel>
            {
                TotalDiscountedSales = first.TotalDiscountedSales,
                TotalSales = first.TotalSales,
                Records = new StaticPagedList<BookingReportViewModel>(reports, pageNumber, pageSize, first.TotalCount)
            };
        }

        public async Task<IEnumerable<ChannelBookingSalesSummaryViewModel>> GetChannelSalesSummaryBookings(
            int? channel,
            int? status,
            string startDate,
            string endDate,
            int? terminalId
            )
        {
            var dbResults = await _unitOfWork
             .GetDbContext<ApplicationDbContext>()
             .Database.ExecuteSqlToObject<ChannelBookingSalesSummaryDto>(@"Exec Sp_ChannelbookingSalesSummary",
              startDate, endDate, status, channel, terminalId);

            return dbResults.Select(u => (ChannelBookingSalesSummaryViewModel)u);
        }

        public async Task<AdvancedBookingSalesSummary<AdvancedBookingReportViewModel>> AdvancedBooking(
            int pageNumber,
            int pageSize,
            int? employeeId,
            string startDate,
            string endDate,
            int? terminalId
            )
        {
            var dbResults = await _unitOfWork
             .GetDbContext<ApplicationDbContext>()
             .Database.ExecuteSqlToObject<AdvancedBookingReportDto>(@"Exec Sp_AdvancedBookingSalesSummary",
             pageNumber, pageSize, startDate, endDate, employeeId, terminalId);

            var first = dbResults.FirstOrDefault() ?? new AdvancedBookingReportDto();

            var reports = dbResults.Select(u => (AdvancedBookingReportViewModel)u);

            return new AdvancedBookingSalesSummary<AdvancedBookingReportViewModel>
            {
                PosSales = first.PosSales,
                CashSales = first.CashSales,
                TotalSales = first.TotalSales,
                Records = new StaticPagedList<AdvancedBookingReportViewModel>(reports, pageNumber, pageSize, first.TotalCount)
            };
        }


        private Task<IQueryable<SalesReportCacheDTO>> GetSalesReportAsync(SalesReportQueryDTO queryDto)
        {
            DateTime StartDate = queryDto.StartDate.GetValueOrDefault().Date;
            DateTime EndDate = queryDto.EndDate?.Date ?? StartDate;

            var sales = from seatmanagement in _seatManagementService.GetAll()

                        join vehicletripregistration in _vehicleTripRegService.GetAll()
                        on seatmanagement.VehicleTripRegistrationId equals vehicletripregistration.Id

                        where seatmanagement.BookingStatus == BookingStatus.Approved && seatmanagement.TravelStatus == TravelStatus.Travelled

                        orderby seatmanagement.CreationTime

                        select new SalesReportCacheDTO
                        {
                            DateCreated = seatmanagement.CreationTime,
                            ExpectedAmount = seatmanagement.Amount ?? 0,
                            Discount = seatmanagement.Discount,
                            ActualAmount = (seatmanagement.Amount ?? 0) - seatmanagement.Discount,
                            PartCash = seatmanagement.PartCash ?? 0.00m,
                            BookingStatus = seatmanagement.BookingStatus,
                            BusRegistrationNumber = seatmanagement.VehicleTripRegistration.PhysicalBusRegistrationNumber,
                            PaymentMethod = seatmanagement.PaymentMethod,
                            BookingTypes = seatmanagement.BookingType,
                            DepartureDate = seatmanagement.VehicleTripRegistration != null ? seatmanagement.VehicleTripRegistration.DepartureDate : DateTime.MinValue,
                            RouteName = (seatmanagement.SubRoute == null) ? seatmanagement.Route.Name : seatmanagement.SubRoute.Name,
                            RouteId = (int)seatmanagement.RouteId,
                            TerminalId = seatmanagement.Route.DepartureTerminalId,
                            TerminalName = seatmanagement.Route.DepartureTerminal.Name,
                            Trip = seatmanagement.VehicleTripRegistration != null ? seatmanagement.VehicleTripRegistration.Trip.DepartureTime : null,
                            TripId = seatmanagement.VehicleTripRegistration != null ? seatmanagement.VehicleTripRegistration.TripId : Guid.Empty,
                            CreatedBy = seatmanagement.CreatedBy,
                            CaptainCode = seatmanagement.VehicleTripRegistration != null ? seatmanagement.VehicleTripRegistration.DriverCode : null,
                            DepartureTime = seatmanagement.VehicleTripRegistration != null ? seatmanagement.VehicleTripRegistration.Trip.DepartureTime : null,
                            vehicleTripId = seatmanagement.VehicleTripRegistrationId,
                            RefCode = seatmanagement.BookingReferenceCode,
                            StateId = seatmanagement.Route.DepartureTerminal.StateId,
                            StateName = seatmanagement.Route.DepartureTerminal.State.Name
                        };

            if (queryDto.StateId.ToString().Length > 0 && queryDto.StateId > 0)
            {
                sales = sales.Where(x => x.StateId == queryDto.StateId);
            }

            if (queryDto.TerminalId.ToString().Length > 0 && queryDto.TerminalId > 0)
            {
                sales = sales.Where(x => x.TerminalId == queryDto.TerminalId);
            }

            if (queryDto.CreatedDate.HasValue)
            {
                DateTime CreatedDate = queryDto.CreatedDate.GetValueOrDefault().Date;
                var nextDay = CreatedDate.AddDays(1).Date;
                sales = sales.Where(x => x.DepartureDate >= CreatedDate && x.DepartureDate < nextDay);
            }

            if (queryDto.StartDate.HasValue && !queryDto.EndDate.HasValue)
            {
                var nextDay = queryDto.StartDate.GetValueOrDefault().AddDays(1).Date;
                sales = sales.Where(x => x.DepartureDate >= StartDate && x.DepartureDate < nextDay);
            }

            if (queryDto.EndDate.HasValue && !queryDto.StartDate.HasValue)
            {
                sales = sales.Where(x => x.DepartureDate <= EndDate);
            }

            if (queryDto.StartDate.HasValue && queryDto.EndDate.HasValue)
            {
                if (queryDto.StartDate.Equals(queryDto.EndDate))
                {
                    var nextDay = queryDto.StartDate.GetValueOrDefault().AddDays(1).Date;
                    sales = sales.Where(x => x.DepartureDate >= StartDate && x.DepartureDate < nextDay);
                }
                else
                {
                    sales = sales.Where(x => x.DepartureDate >= StartDate && x.DepartureDate <= EndDate);
                }
            }

            if (!string.IsNullOrWhiteSpace(queryDto.VehicleRegNo))
            {
                sales = sales.Where(x => x.BusRegistrationNumber.ToLower() == queryDto.VehicleRegNo.ToLower());
            }

            if (queryDto.BookingTypes.HasValue && queryDto.BookingTypes >= 0)
            {
                sales = sales.Where(x => x.BookingTypes == queryDto.BookingTypes);
            }

            if (queryDto.PaymentMethod.HasValue && queryDto.PaymentMethod >= 0)
            {
                sales = sales.Where(x => x.PaymentMethod == queryDto.PaymentMethod);
            }

            if (queryDto.OBREmail != null && queryDto.OBREmail.Length > 0)
            {
                sales = sales.Where(x => x.CreatedBy == queryDto.OBREmail);
            }

            if (queryDto.TicketerEmail != null && queryDto.TicketerEmail.Length > 0)
            {
                sales = sales.Where(x => x.CreatedBy == queryDto.TicketerEmail);
            }

            return Task.FromResult(sales.AsQueryable().AsNoTracking());
        }

        public async Task<List<DLSalesReportDTO>> GetSalesReportByDateAsync(SalesReportQueryDTO queryDto)
        {
            var sales = await GetSalesReportAsync(queryDto);

            var grouped = from sale in sales
                          group sale by sale.TripId into terminalGroup
                          select new DLSalesReportDTO
                          {
                              ActualAmount = terminalGroup.Sum(x => x.ActualAmount) - terminalGroup.Sum(x => x.Discount),
                              Discount = terminalGroup.Sum(x => x.Discount),
                              ExpectedAmount = terminalGroup.Sum(x => x.ActualAmount),
                              TerminalId = terminalGroup.FirstOrDefault().TerminalId,
                              Terminal = terminalGroup.FirstOrDefault().TerminalName,
                              DepartureDate = terminalGroup.FirstOrDefault().DepartureDate,
                              BusRegistrationNumber = terminalGroup.FirstOrDefault().BusRegistrationNumber,
                              CaptainCode = terminalGroup.FirstOrDefault().CaptainCode,
                              DateCreated = terminalGroup.FirstOrDefault().DateCreated,
                              RouteId = terminalGroup.FirstOrDefault().RouteId,
                              Route = terminalGroup.FirstOrDefault().RouteName,
                              VehicleTripId = terminalGroup.FirstOrDefault().vehicleTripId,
                              DepartureTime = terminalGroup.FirstOrDefault().DepartureTime,
                              TripId = terminalGroup.Key,
                              CreatedBy = terminalGroup.FirstOrDefault().CreatedBy,
                              BookingStatus = terminalGroup.FirstOrDefault().BookingStatus,
                              PaymentMethod = terminalGroup.FirstOrDefault().PaymentMethod,
                              BookingTypes = terminalGroup.FirstOrDefault().BookingTypes,
                              RefCode = terminalGroup.FirstOrDefault().RefCode
                          };

            return await grouped.AsNoTracking().ToListAsync();
        }

        public async Task<decimal> GetTotalSalesByTicketer(string username)
        {
            var queryDto = new SalesReportQueryDTO
            {
                TicketerEmail = username,
                BookingTypes = BookingTypes.Terminal,

                CreatedDate = DateTime.Today
            };
            decimal result = 0;

            var sales = await GetSalesReportByDateAsync(queryDto);

            foreach (var sale in sales)
            {
                result += sale.ActualAmount;
            }
            return result;
        }

        public async Task<decimal> GetTotalPosByTicketer(string username)
        {
            var queryDto = new SalesReportQueryDTO
            {
                TicketerEmail = username,
                BookingTypes = BookingTypes.Terminal,
                PaymentMethod = PaymentMethod.Pos,

                CreatedDate = DateTime.Today
            };
            decimal result = 0;

            var sales = await GetSalesReportByDateAsync(queryDto);

            foreach (var sale in sales)
            {
                result += sale.ActualAmount;
            }
            return result;
        }

        public async Task<decimal> GetTotalCashByTicketer(string username)
        {
            var queryDto = new SalesReportQueryDTO
            {
                TicketerEmail = username,
                BookingTypes = BookingTypes.Terminal,
                PaymentMethod = PaymentMethod.Cash,

                CreatedDate = DateTime.Today
            };
            decimal result = 0;

            var sales = await GetSalesReportByDateAsync(queryDto);

            foreach (var sale in sales)
            {
                result += sale.ActualAmount;
            }
            return result;
        }

        public async Task<List<DLSalesReportDTO>> GetTicketerDailySales(string email)
        {
            var queryDto = new SalesReportQueryDTO
            {
                TicketerEmail = email,
                BookingTypes = BookingTypes.Terminal,
                CreatedDate = DateTime.Now
            };

            var sales = await GetSalesReportAsync(queryDto);

            var grouped = from seatmanagement in sales
                          select new DLSalesReportDTO
                          {
                              DateCreated = seatmanagement.DateCreated,
                              ExpectedAmount = seatmanagement.ExpectedAmount,
                              PartCash = seatmanagement.PartCash,
                              Discount = seatmanagement.Discount,
                              ActualAmount = seatmanagement.ActualAmount,
                              BookingStatus = seatmanagement.BookingStatus,
                              BusRegistrationNumber = seatmanagement.BusRegistrationNumber,
                              PaymentMethod = seatmanagement.PaymentMethod,
                              BookingTypes = seatmanagement.BookingTypes,
                              DepartureDate = seatmanagement.DepartureDate,
                              Route = seatmanagement.RouteName,
                              TerminalId = seatmanagement.TerminalId,
                              Terminal = seatmanagement.TerminalName,
                              Trip = seatmanagement.Trip,
                              TripId = seatmanagement.TripId,
                              CreatedBy = seatmanagement.CreatedBy,
                              CaptainCode = seatmanagement.CaptainCode,
                              DepartureTime = seatmanagement.DepartureTime,
                              RouteId = seatmanagement.RouteId,
                              VehicleTripId = seatmanagement.vehicleTripId,
                              RefCode = seatmanagement.RefCode
                          };

            return await grouped.AsNoTracking().ToListAsync();
        }

        public async Task<List<DLSalesReportDTO>> GetTicketerDailyPosSales(string email)
        {
            var queryDto = new SalesReportQueryDTO
            {
                TicketerEmail = email,
                BookingTypes = BookingTypes.Terminal,
                PaymentMethod = PaymentMethod.Pos,
                CreatedDate = DateTime.Now
            };

            var possales = (await GetSalesReportAsync(queryDto))
                .Select(seatmanagement => new DLSalesReportDTO
                {
                    DateCreated = seatmanagement.DateCreated,
                    ExpectedAmount = seatmanagement.ExpectedAmount,
                    PartCash = seatmanagement.PartCash,
                    Discount = seatmanagement.Discount,
                    ActualAmount = seatmanagement.ActualAmount,
                    BookingStatus = seatmanagement.BookingStatus,
                    BusRegistrationNumber = seatmanagement.BusRegistrationNumber,
                    PaymentMethod = seatmanagement.PaymentMethod,
                    BookingTypes = seatmanagement.BookingTypes,
                    DepartureDate = seatmanagement.DepartureDate,
                    Route = seatmanagement.RouteName,
                    TerminalId = seatmanagement.TerminalId,
                    Terminal = seatmanagement.TerminalName,
                    Trip = seatmanagement.Trip,
                    TripId = seatmanagement.TripId,
                    CreatedBy = seatmanagement.CreatedBy,
                    CaptainCode = seatmanagement.CaptainCode,
                    DepartureTime = seatmanagement.DepartureTime,
                    RouteId = seatmanagement.RouteId,
                    VehicleTripId = seatmanagement.vehicleTripId,
                    RefCode = seatmanagement.RefCode
                }).AsNoTracking().ToList();



            queryDto.PaymentMethod = PaymentMethod.CashAndPos;

            var partPossales = (await GetSalesReportAsync(queryDto))
                .Select(seatmanagement => new DLSalesReportDTO
                {
                    DateCreated = seatmanagement.DateCreated,
                    ExpectedAmount = seatmanagement.ExpectedAmount,
                    PartCash = seatmanagement.PartCash,
                    Discount = seatmanagement.Discount,
                    ActualAmount = seatmanagement.ActualAmount,
                    BookingStatus = seatmanagement.BookingStatus,
                    BusRegistrationNumber = seatmanagement.BusRegistrationNumber,
                    PaymentMethod = seatmanagement.PaymentMethod,
                    BookingTypes = seatmanagement.BookingTypes,
                    DepartureDate = seatmanagement.DepartureDate,
                    Route = seatmanagement.RouteName,
                    TerminalId = seatmanagement.TerminalId,
                    Terminal = seatmanagement.TerminalName,
                    Trip = seatmanagement.Trip,
                    TripId = seatmanagement.TripId,
                    CreatedBy = seatmanagement.CreatedBy,
                    CaptainCode = seatmanagement.CaptainCode,
                    DepartureTime = seatmanagement.DepartureTime,
                    RouteId = seatmanagement.RouteId,
                    VehicleTripId = seatmanagement.vehicleTripId,
                    RefCode = seatmanagement.RefCode
                }).AsNoTracking().ToList();

            possales.AddRange(partPossales);

            return possales;
        }

        public async Task<List<DLSalesReportDTO>> GetTicketerDailyCashSales(string email)
        {
            var queryDto = new SalesReportQueryDTO
            {
                TicketerEmail = email,
                BookingTypes = BookingTypes.Terminal,
                PaymentMethod = PaymentMethod.Cash,
                CreatedDate = DateTime.Now
            };

            var cashsales = (await GetSalesReportAsync(queryDto))
                .Select(seatmanagement => new DLSalesReportDTO
                {
                    DateCreated = seatmanagement.DateCreated,
                    ExpectedAmount = seatmanagement.ExpectedAmount,
                    PartCash = seatmanagement.PartCash,
                    Discount = seatmanagement.Discount,
                    ActualAmount = seatmanagement.ActualAmount,
                    BookingStatus = seatmanagement.BookingStatus,
                    BusRegistrationNumber = seatmanagement.BusRegistrationNumber,
                    PaymentMethod = seatmanagement.PaymentMethod,
                    BookingTypes = seatmanagement.BookingTypes,
                    DepartureDate = seatmanagement.DepartureDate,
                    Route = seatmanagement.RouteName,
                    TerminalId = seatmanagement.TerminalId,
                    Terminal = seatmanagement.TerminalName,
                    Trip = seatmanagement.Trip,
                    TripId = seatmanagement.TripId,
                    CreatedBy = seatmanagement.CreatedBy,
                    CaptainCode = seatmanagement.CaptainCode,
                    DepartureTime = seatmanagement.DepartureTime,
                    RouteId = seatmanagement.RouteId,
                    VehicleTripId = seatmanagement.vehicleTripId,
                    RefCode = seatmanagement.RefCode
                }).AsNoTracking().ToList();



            queryDto.PaymentMethod = PaymentMethod.CashAndPos;

            var partcashsales = (await GetSalesReportAsync(queryDto))
                .Select(seatmanagement => new DLSalesReportDTO
                {
                    DateCreated = seatmanagement.DateCreated,
                    ExpectedAmount = seatmanagement.ExpectedAmount,
                    PartCash = seatmanagement.PartCash,
                    Discount = seatmanagement.Discount,
                    ActualAmount = seatmanagement.ActualAmount,
                    BookingStatus = seatmanagement.BookingStatus,
                    BusRegistrationNumber = seatmanagement.BusRegistrationNumber,
                    PaymentMethod = seatmanagement.PaymentMethod,
                    BookingTypes = seatmanagement.BookingTypes,
                    DepartureDate = seatmanagement.DepartureDate,
                    Route = seatmanagement.RouteName,
                    TerminalId = seatmanagement.TerminalId,
                    Terminal = seatmanagement.TerminalName,
                    Trip = seatmanagement.Trip,
                    TripId = seatmanagement.TripId,
                    CreatedBy = seatmanagement.CreatedBy,
                    CaptainCode = seatmanagement.CaptainCode,
                    DepartureTime = seatmanagement.DepartureTime,
                    RouteId = seatmanagement.RouteId,
                    VehicleTripId = seatmanagement.vehicleTripId,
                    RefCode = seatmanagement.RefCode
                }).AsNoTracking().ToList();

            cashsales.AddRange(partcashsales);

            return cashsales;
        }
    }
}