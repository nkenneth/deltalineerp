﻿namespace LME.Core.Entities.Enums
{
    public enum RescheduleStatus
    {
        NotReshcdule,
        Pending,
        PayAtTerminal,
        Accepted,
        Declined

    }
}