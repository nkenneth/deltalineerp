﻿using System;

namespace LME.Core.Entities.Enums
{
    [Flags]
    public enum BookingTypes
    {
        Terminal,
        Advanced,
        Online,
        All,
        BookOnHold,
        Agent
    }
}