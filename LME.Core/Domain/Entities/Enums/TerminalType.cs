﻿namespace LME.Core.Entities.Enums
{
    public enum TerminalType
    {
        Physical,
        Virtual
    }
}