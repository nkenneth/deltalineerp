﻿using LME.Core.Entities.Common;

namespace LME.Core.Entities
{
    public class IdentificationType: FullAuditedEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}