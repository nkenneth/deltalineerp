﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace LME.Data.efCore.Mapping
{
    class RegionConfig : IEntityTypeConfiguration<Region>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Region> builder)
        {
            builder.ToTable(nameof(Region));
        }
    }
}
