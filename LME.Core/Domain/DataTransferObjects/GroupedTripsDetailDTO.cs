﻿using LME.Core.Common.Enums;
using System.Collections.Generic;

namespace LME.Core.Domain.DataTransferObjects
{
    public class GroupedTripsDetailDTO
    {
        public TripType TripType { get; set; }
        public List<AvailableTripDetailDTO> Departures { get; set; }
        public List<AvailableTripDetailDTO> Arrivals { get; set; }
    }
}