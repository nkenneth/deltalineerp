﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class AlterSalesReport2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sql = @"ALTER   PROCEDURE [dbo].[Sp_Salesreport] 

	               @RouteId int
	              , @TerminalId int 
	              , @StateId int 
	              , @CreatedBy nvarchar(100),
	              @PaymentMethod int,
	              @StartDate DateTime ,
	              @EndDate DateTime
            AS
            BEGIN
	            -- SET NOCOUNT ON added to prevent extra result sets from
	            -- interfering with SELECT statements.
	            SET NOCOUNT ON;

                -- Insert statements for procedure here
	            select Amount, RouteId, Route, DepartureTerminalId  , DepartureTerminal, CreatedBy, StateId, Region, State, BookingType from 
		            (select sum(seat.Amount) as Amount, 
		            rt.Id as RouteId , 
		            rt.Name as Route , 
		            term.Id as DepartureTerminalId,
	            term.Name as DepartureTerminal ,
		            seat.CreatedBy, stat.Id as StateId, stat.Name as State, reg.Id as RegionId, reg.Name as Region, seat.BookingType as BookingType
	            from SeatManagement seat
	             join Route rt
	             on rt.Id = seat.RouteId
	             join Terminal term
	             on term.Id = rt.DepartureTerminalId
	             Join State stat
	             on term.StateId = stat.Id
	             Join Region reg
	             on reg.Id = stat.RegionId
	             where  (seat.CreationTime between @StartDate and @EndDate)
	             group by seat.CreatedBy, rt.id, rt.Name, term.Id, term.Name, stat.Id, stat.Name, reg.Id, reg.Name, seat.BookingType) as result
	             where (@RouteId is null or RouteId = @RouteId) and 
			            (@StateId is null or StateId = @StateId) and
			            (@TerminalId is null or DepartureTerminalId = @TerminalId) and
			            (@CreatedBy is null or CreatedBy =  @CreatedBy) and
			            (@PaymentMethod is null or BookingType =  @PaymentMethod) 

            END";

            migrationBuilder.Sql(sql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {


        }
    }
}
