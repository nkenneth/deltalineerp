﻿using LME.Core.Entities.Common;

namespace LME.Core.Entities
{
    public class WalletNumber : FullAuditedEntity
    {
        public string WalletPan { get; set; }
        public bool IsActive { get; set; }
    }
}