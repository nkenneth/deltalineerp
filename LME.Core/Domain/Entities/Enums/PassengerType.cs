﻿namespace LME.Core.Entities.Enums
{
   public enum PassengerType
    {
        Adult,
        Children
    }
}