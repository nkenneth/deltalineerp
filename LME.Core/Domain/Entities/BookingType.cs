﻿namespace LME.Core.Entities
{
    public class BookingType:Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}