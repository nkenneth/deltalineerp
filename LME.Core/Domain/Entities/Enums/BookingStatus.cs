﻿namespace LME.Core.Entities.Enums
{
   public enum BookingStatus
    {
        Pending,
        Approved,
        Cancelled,
        Created,
        Declined,
        Expired,
        Failed,
        OnLock,
        OnPayment,
        Ongoing,
        Abandoned,
        Refunded,
        Reversed,
        TransactionError,
        Unsuccessful,
        GtbCancelled,
        Suspended
    }
}