﻿using LME.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace LME.Core.Domain.DataTransferObjects
{
    public class DateModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Keyword { get; set; }
        public BookingTypes? BookingType { get; set; }
        public int Id { get; set; }
    }

    public class SearchDTO
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Driver { get; set; }
        public string Code { get; set; }
        public string PhysicalBusRegistrationNumber { get; set; }

    }

    public class SalesReportQueryDTO {
        public DateTime? CreatedDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public PaymentMethod? PaymentMethod { get; set; }
        public BookingTypes? BookingTypes { get; set; }
        public int TerminalId { get; set; }
        public string TerminalName { get; set; }
        public int RouteId { get; set; }
        public string RouteName { get; set; }
        public string OBREmail { get; set; }
        public string TicketerEmail { get; set; }
        public int VehicleId { get; set; }
        public string VehicleRegNo { get; set; }
        public int StateId { get; set; }
    }

    public class SalesReportCacheDTO
    {
        public DateTime DateCreated { get; set; }
        public decimal ExpectedAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal ActualAmount { get; set; }
        public string Terminal { get; set; }
        public string CaptainCode { get; set; }
        public string DepartureTime { get; set; }
        public string BusRegistrationNumber { get; set; }
        public BookingStatus BookingStatus { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public BookingTypes BookingTypes { get; set; }

        public string RouteName { get; set; }
        public int RouteId { get; set; }
        public int TerminalId { get; set; }
        public string TerminalName { get; set; }
        public DateTime? DepartureDate { get; set; }
        public Guid TripId { get; set; }
        public string Trip { get; set; }
        public string CreatedBy { get; set; }
        public Guid? vehicleTripId { get; set; }
        public string RefCode { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public decimal PartCash { get; set; }

        public DateTime? CreatedDate { get; set; }

    }
}
