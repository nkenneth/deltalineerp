﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class Sp_BookedBuses2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sql = @"Alter PROCEDURE Sp_BookedBusesReports
	                            @BookingType int, 
		                        @PhysicalBusRegisterationNumber nvarchar(20),
	                            @StartDate DateTime,
		                        @EndDate DateTime
                        AS
                        BEGIN
                            -- SET NOCOUNT ON added to prevent extra result sets from
                            -- interfering with SELECT statements.
                            SET NOCOUNT ON

                            -- Insert statements for procedure here
                            Select distinct(vtr.Id)  as VehicleTripRegistrationId, vtr.PhysicalBusRegistrationNumber , 
		                        count(sm.BookingReferenceCode) over (Partition by vtr.Id)  as SeatsBooked,  rt.Name as RouteName,
		                        ((select NumberOfSeats from VehicleModel where Id = vtr.VehicleModelId) - count(sm.BookingReferenceCode) over (Partition by vtr.Id)) as AvailableSeats,
		                        vtr.DepartureDate as DepartureDate, [TotalCount]= COUNT(*) OVER(),
								(count(sm.BookingReferenceCode) over (Partition by vtr.Id) * sm.Amount) as Revenue ,
		                        tr.DepartureTime as DepartureTime
		                        from SeatManagement sm 
		                        Join VehicleTripRegistration vtr
		                        on vtr.Id = sm.VehicleTripRegistrationId
                                --join JourneyManagement jm
		                        --on jm.VehicleTripRegistrationId = vtr.Id
		                        Join Trip tr 
		                        on tr.Id = vtr.TripId
                                Join Route rt 
								on rt.Id = tr.RouteId
		                        where (@BookingType is null or sm.BookingType =  @BookingType)
		                        and 
		                        (@PhysicalBusRegisterationNumber is null or vtr.PhysicalBusRegistrationNumber = @PhysicalBusRegisterationNumber)
		                        And (vtr.DepartureDate between @StartDate and @EndDate)
                        END
                        GO";
            migrationBuilder.Sql(sql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
