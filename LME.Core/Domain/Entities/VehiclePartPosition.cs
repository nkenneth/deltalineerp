﻿using LME.Core.Entities.Common;
using System;

namespace LME.Core.Entities
{
    public class VehiclePartPosition: FullAuditedEntity<Guid>
    {
        public string Position { get; set; }
        public VehiclePart VehiclePart { get; set; }
        public int VehiclePartId { get; set; }
    }
}