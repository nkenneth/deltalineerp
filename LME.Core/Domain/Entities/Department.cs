﻿using LME.Core.Entities.Common;

namespace LME.Core.Entities
{
    public class Department : FullAuditedEntity
    {
        public string Name { get; set; }
    }
}
