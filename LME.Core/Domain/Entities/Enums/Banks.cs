﻿namespace LME.Core.Entities.Enums
{
    public enum Banks
    {
        FirstBank,
        GTBank,
        AccessBank,
        UBA,
        ZenithBAnk,
        FidelityBank,
        DiamondBank,
        FCMB,
        StanbicIBTC,
        StandardChatterd,
        UnityBank,
        UnionBank,
        PolarisBank,
        Ecobank,
        SterlingBank,
        KeystoneBank
    }
}