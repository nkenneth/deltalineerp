﻿namespace LME.Core.Entities.Enums
{
    public enum DriverType
    {
        Permanent,
        Handover,
        Owner,
        Virtual
    }
}