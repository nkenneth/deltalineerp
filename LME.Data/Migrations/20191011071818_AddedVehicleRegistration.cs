﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class AddedVehicleRegistration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "VehicleReg",
                table: "MtuReportModel",
                newName: "VehicleId");

            migrationBuilder.AddColumn<string>(
                name: "RegistrationNumber",
                table: "MtuReportModel",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RegistrationNumber",
                table: "MtuReportModel");

            migrationBuilder.RenameColumn(
                name: "VehicleId",
                table: "MtuReportModel",
                newName: "VehicleReg");
        }
    }
}
