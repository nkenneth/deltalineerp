﻿namespace LME.Core.Entities.Enums
{
    public enum AccountingStatus
    {
        Pending,
        Accepted,
        Declined
    }
}