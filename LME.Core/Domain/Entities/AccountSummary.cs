﻿using LME.Core.Entities.Common;
using System;

namespace LME.Core.Entities
{
    public class AccountSummary : FullAuditedEntity<Guid>
    {
        public AccountSummary()
        {
        }

        public string AccountName { get; set; }
        public double Balance { get; set; }
    }
}