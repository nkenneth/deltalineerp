﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class userotp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ManifestManagement");

            migrationBuilder.AddColumn<string>(
                name: "OTP",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Manifest",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    NumberOfSeats = table.Column<int>(nullable: false),
                    IsPrinted = table.Column<bool>(nullable: false),
                    Amount = table.Column<decimal>(nullable: true),
                    Dispatch = table.Column<decimal>(nullable: true),
                    ManifestPrintedTime = table.Column<DateTime>(nullable: true),
                    VehicleTripRegistrationId = table.Column<Guid>(nullable: false),
                    VehicleModelId = table.Column<int>(nullable: true),
                    Employee = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Manifest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Manifest_VehicleModel_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Manifest_VehicleTripRegistration_VehicleTripRegistrationId",
                        column: x => x.VehicleTripRegistrationId,
                        principalTable: "VehicleTripRegistration",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Manifest_VehicleModelId",
                table: "Manifest",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Manifest_VehicleTripRegistrationId",
                table: "Manifest",
                column: "VehicleTripRegistrationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Manifest");

            migrationBuilder.DropColumn(
                name: "OTP",
                table: "AspNetUsers");

            migrationBuilder.CreateTable(
                name: "ManifestManagement",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Amount = table.Column<decimal>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Dispatch = table.Column<decimal>(nullable: true),
                    Employee = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsPrinted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ManifestPrintedTime = table.Column<DateTime>(nullable: true),
                    NumberOfSeats = table.Column<int>(nullable: false),
                    VehicleModelId = table.Column<int>(nullable: true),
                    VehicleTripRegistrationId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManifestManagement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ManifestManagement_VehicleModel_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ManifestManagement_VehicleTripRegistration_VehicleTripRegistrationId",
                        column: x => x.VehicleTripRegistrationId,
                        principalTable: "VehicleTripRegistration",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ManifestManagement_VehicleModelId",
                table: "ManifestManagement",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ManifestManagement_VehicleTripRegistrationId",
                table: "ManifestManagement",
                column: "VehicleTripRegistrationId");
        }
    }
}
