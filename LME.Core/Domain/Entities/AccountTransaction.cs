﻿using LME.Core.Entities.Common;
using LME.Core.Entities.Enums;
using System;

namespace LME.Core.Entities
{
    public class AccountTransaction : FullAuditedEntity<Guid>
    {
        public AccountTransaction()
        {
        }

        public double Amount { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Narration { get; set; }
        public AccountType AccountType { get; set; }
        public TransactionType TransactionType { get; set; }
        public Guid TransactionSourceId { get; set; }
    }
}