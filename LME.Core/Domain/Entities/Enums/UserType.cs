﻿namespace LME.Core.Entities.Enums
{
    public enum UserType
    {
        Administrator,
        Employee,
        Partner,
        Customer,
        Captain
    }
}