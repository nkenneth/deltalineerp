﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class BookingTypeConfig : IEntityTypeConfiguration<BookingType>
    {
        public void Configure(EntityTypeBuilder<BookingType> builder)
        {
            builder.ToTable(nameof(BookingType));

        }
    }
}
