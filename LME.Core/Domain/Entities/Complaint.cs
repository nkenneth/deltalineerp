﻿using LME.Core.Domain.Entities.Enums;
using LME.Core.Entities.Common;
using System;

namespace LME.Core.Entities
{
    public class Complaint : FullAuditedEntity
    {

        public string FullName { get; set; }
        public string Email { get; set; }

        public ComplaintTypes ComplaintType { get; set; }

        public PriorityLevel PriorityLevel { get; set; }

        public string BookingReference { get; set; }
        public string Message { get; set; }

        public DateTime TransDate { get; set; }
        public bool Responded { get; set; }
        public string RepliedMessage { get; set; }
    }
}
