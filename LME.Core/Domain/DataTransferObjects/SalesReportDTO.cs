﻿using LME.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace LME.Core.Domain.DataTransferObjects
{
    public class SalesReportDTO
    {
        public Guid VehicleTripRegistrationId { get; set; }
        public string DriverName { get; set; }
        public string RouteName { get; set; }
        public string PhysicalBusRegistrationNumber { get; set; }
        public int TotalPassengers { get; set; }
        public decimal AmountTotal { get; set; }
        public decimal TotalOnlineSales { get; set; }
        public decimal TotalAdvancedSales { get; set; }
        public decimal TotalTerminalSales { get; set; }
        public decimal TotalSales { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public class DLSalesReportDTO
    {
        public DateTime DateCreated { get; set; }
        public decimal ExpectedAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal ActualAmount { get; set; }
        public string CaptainCode { get; set; }
        public string DepartureTime { get; set; }
        public string BusRegistrationNumber { get; set; }
        public BookingStatus BookingStatus { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public BookingTypes BookingTypes { get; set; }
        public string Route { get; set; }
        public int RouteId { get; set; }
        public int TerminalId { get; set; }
        public string Terminal { get; set; }
        public DateTime? DepartureDate { get; set; }
        public Guid TripId { get; set; }
        public string Trip { get; set; }
        public string CreatedBy { get; set; }
        public Guid? VehicleTripId { get; set; }
        public string RefCode { get; set; }
        public int StateId { get; set; }
        public string State { get; set; }
        public decimal PartCash { get; set; }
    }
}
