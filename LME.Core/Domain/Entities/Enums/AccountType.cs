﻿namespace LME.Core.Entities.Enums
{
    public enum AccountType
    {
        InventoryAccount,
        BookingAccount,
        MaintenanceAccount,
        CashRemittantAccount,
        ExpenseAccount,
        BankAccount,
        OtherIncome
    }
}