﻿namespace LME.Core.Entities
{
    public class PaymentGatewayStatus:Entity
    {
        public string Gateway { get; set; }
        public bool Status { get; set; }
    }
}
