﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class dispatchsource : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DispatchSource",
                table: "Manifest",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DispatchSource",
                table: "Manifest");
        }
    }
}
