﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class changedVehicleinfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VehicleStatus",
                table: "Vehicle");

            migrationBuilder.RenameColumn(
                name: "VehicleType",
                table: "Vehicle",
                newName: "Type");

            migrationBuilder.RenameColumn(
                name: "VehicleRegistrationNumber",
                table: "Vehicle",
                newName: "RegistrationNumber");

            migrationBuilder.RenameColumn(
                name: "VehicleIMEINumber",
                table: "Vehicle",
                newName: "IMEINumber");

            migrationBuilder.RenameColumn(
                name: "VehicleEngineNumber",
                table: "Vehicle",
                newName: "EngineNumber");

            migrationBuilder.RenameColumn(
                name: "VehicleChasisNumber",
                table: "Vehicle",
                newName: "ChasisNumber");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Vehicle",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Vehicle");

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "Vehicle",
                newName: "VehicleType");

            migrationBuilder.RenameColumn(
                name: "RegistrationNumber",
                table: "Vehicle",
                newName: "VehicleRegistrationNumber");

            migrationBuilder.RenameColumn(
                name: "IMEINumber",
                table: "Vehicle",
                newName: "VehicleIMEINumber");

            migrationBuilder.RenameColumn(
                name: "EngineNumber",
                table: "Vehicle",
                newName: "VehicleEngineNumber");

            migrationBuilder.RenameColumn(
                name: "ChasisNumber",
                table: "Vehicle",
                newName: "VehicleChasisNumber");

            migrationBuilder.AddColumn<int>(
                name: "VehicleStatus",
                table: "Vehicle",
                nullable: false,
                defaultValue: 0);
        }
    }
}
