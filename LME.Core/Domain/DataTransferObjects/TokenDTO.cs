﻿using System;

namespace LME.Core.DataTransferObjects
{
    public class TokenDTO
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public DateTime Expires { get; set; }
    }

    public class TerminalTokenDto
    {
        public string TokenToValidate { get; set; }
        public int RegionId { get; set; }
        public int TerminalId { get; set; }
    }
}