﻿using LME.Core.Common.Enums;
using LME.Core.Common.Extensions;
using System;
using System.ComponentModel.DataAnnotations;

namespace LME.WebAPI.ViewModels
{
    public class PayStackResponseModel
    {
        public string email { get; set; }

        public int amount { get; set; }

        public string RefCode { get; set; }

        public string PayStackReference { get; set; }
    }
}
