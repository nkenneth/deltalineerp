﻿namespace LME.Core.Entities.Enums
{
    public enum RerouteMode
    {
        Admin,
        Android,
        IOS,
        OnlineWebsite,
        OnlineMobile
    }
}