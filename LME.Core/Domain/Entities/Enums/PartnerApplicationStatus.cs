﻿namespace LME.Core.Entities.Enums
{
    public enum PartnerApplicationStatus
    {
        Pending,
        Referred,
        Approved,
        Rejected
    }
}