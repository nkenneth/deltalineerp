﻿namespace LME.Core.Entities.Enums
{
    public enum PartnerType
    {
        Corporate,
        Individual
    }
}