﻿namespace LME.Core.Entities
{
    public class Bank : Entity
    {
        public string AccountType { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string Name { get; set; }
    }
}