﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LME.Core.Domain.Entities.Enums
{
    public enum PriorityLevel
    {
        High,
        Low,
        Normal,
        Medium

    }
}
