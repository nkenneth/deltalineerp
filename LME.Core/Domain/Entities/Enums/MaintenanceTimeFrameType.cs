﻿namespace LME.Core.Entities.Enums
{
    public enum MaintenanceTimeFrameType
    {
        Days,
        Mileage
    }
}