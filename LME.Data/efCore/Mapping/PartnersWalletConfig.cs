﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class PartnersWalletConfig : IEntityTypeConfiguration<PartnersWallet>
    {
        public void Configure(EntityTypeBuilder<PartnersWallet> builder)
        {
            builder.HasIndex(x => x.WalletNumber).IsUnique();
            builder.ToTable(nameof(PartnersWallet));

        }
    }
}
