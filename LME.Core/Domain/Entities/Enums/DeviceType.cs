﻿namespace LME.Core.Entities.Enums
{
    public enum DeviceType
    {
        Web,
        Android,
        iOS,
        Windows,
        Symbian,
        Tizen
    }
}