﻿namespace LME.Core.Entities.Enums
{
    public enum LoyaltyValidityType
    {
        Days,
        Weeks,
        Months,
        Years
    }
}