﻿namespace LME.Core.Entities.Enums
{
    public enum RouteType
    {
        Short,
        Medium,
        Long,
        BackwardPickup
    }
}