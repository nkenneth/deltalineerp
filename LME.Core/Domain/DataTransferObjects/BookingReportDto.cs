﻿
using IPagedList;
using LME.Core.Entities.Enums;

namespace LME.Core.Domain.DataTransferObjects
{
    public class BookingReportDto
    {
        public int SeatNumber { get; set; }
        public string BookingReferenceCode { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string NextOfKinName { get; set; }
        public decimal Amount { get; set; }
        public decimal DiscountedAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal RerouteFeeDiff { get; set; }
        public string FullName { get; set; }
        public Gender Gender { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeEmail { get; set; }
        public string TerminalName { get; set; }
        public string BoookingDate { get; set; }

        public int TotalCount { get; set; }
        public decimal TotalSales { get; set; }
        public decimal TotalDiscountedSales { get; set; }
        public string Destination { get; set; }
    }

    public class AdvancedBookingReportDto
    {
        public string BookingReferenceCode { get; set; }
        public string VehicleNumber { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public decimal Amount { get; set; }
        public decimal DiscountedAmount { get; set; }
        public string Employee { get; set; }

        public string BookingDate { get; set; }
        public string DepartureDate { get; set; }

        public int TotalCount { get; set; }
        public decimal PosSales { get; set; }
        public decimal CashSales { get; set; }
        public decimal TotalSales { get; set; }
    }

    public class BookingSalesSummary<T>
    {
        public decimal TotalSales { get; set; }
        public decimal TotalDiscountedSales { get; set; }
        public IPagedList<T> Records { get; set; }
    }

    public class AdvancedBookingSalesSummary<T>
    {
        public decimal PosSales { get; set; }
        public decimal CashSales { get; set; }
        public decimal TotalSales { get; set; }
        public IPagedList<T> Records { get; set; }
    }

    public class BookingReportViewModel
    {
        public int SeatNumber { get; set; }
        public string BookingReferenceCode { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string NextOfKinName { get; set; }
        public decimal Amount { get; set; }
        public decimal DiscountedAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal RerouteFeeDiff { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string Terminal { get; set; }
        public string Destination { get; set; }
        public string Date { get; set; }


        public static implicit operator BookingReportViewModel(BookingReportDto source)
        {
            var destination = new BookingReportViewModel
            {
                Amount = source.Amount,
                BookingReferenceCode = source.BookingReferenceCode,
                CustomerPhoneNumber = source.CustomerPhoneNumber,
                Destination = source.Destination,
                Date = source.BoookingDate,
                Discount = source.Discount,
                DiscountedAmount = source.DiscountedAmount,
                FullName = source.FullName,
                Gender = source.Gender.ToString(),
                NextOfKinName = source.NextOfKinName,
                RerouteFeeDiff = source.RerouteFeeDiff,
                SeatNumber = source.SeatNumber,
                Terminal = source.TerminalName,
                 
            };
            return destination;
        }
    }

    public class AdvancedBookingReportViewModel
    {
        public string BookingReferenceCode { get; set; }
        public string VehicleNumber { get; set; }
        public string Employee { get; set; }
        public string PaymentMethod { get; set; }
        public decimal Amount { get; set; }
        public decimal DiscountedAmount { get; set; }
        public string BookingDate { get; set; }
        public string DepartureDate { get; set; }

        public static implicit operator AdvancedBookingReportViewModel(AdvancedBookingReportDto source)
        {
            var destination = new AdvancedBookingReportViewModel
            {
                Amount = source.Amount,
                BookingReferenceCode = source.BookingReferenceCode,
                DiscountedAmount = source.DiscountedAmount,
                BookingDate = source.BookingDate,
                DepartureDate = source.DepartureDate,
                PaymentMethod = source.PaymentMethod.ToString(),
                VehicleNumber = source.VehicleNumber,
                Employee = source.Employee,
            };
            return destination;
        }
    }
}