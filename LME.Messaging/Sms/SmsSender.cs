﻿using System.Threading.Tasks;

namespace LME.Messaging.Sms
{
    public abstract class SMSSender
    {
        public abstract Task SendSmsAsync();
        public abstract void SendSms();
    }
}