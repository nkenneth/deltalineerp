﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class Sp_SalesByBus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"CREATE PROCEDURE Sp_SalesByBus
	                        @StartDate DateTime,
	                        @EndDate DateTime
                        AS
                        BEGIN
	                        -- SET NOCOUNT ON added to prevent extra result sets from
	                        -- interfering with SELECT statements.
	                        SET NOCOUNT ON;

                            -- Insert statements for procedure here
	
			                        Select Distinct  Sum(seatm.Amount) over (Partition by vtr.id) as TotalSales, vtr.Id, vtr.PhysicalBusRegistrationNumber , vtr.DepartureDate, 
					                        (select Coalesce(NumberOfSeats, 0) from Vehicle vehi join VehicleModel model on vehi.VehicleModelId = model.Id where vehi.RegistrationNumber = vtr.PhysicalBusRegistrationNumber)
					                        - (select Count(*)  from SeatManagement where VehicleTripRegistrationId = vtr.Id)
					                          RemainingSeats,(select Count(*)  from SeatManagement where VehicleTripRegistrationId = vtr.Id) NoOfSeats,
					                         ( Select Sum(Amount) from SeatManagement where BookingType = 0 and VehicleTripRegistrationId = seatm.VehicleTripRegistrationId) as TerminalBookingSales,
					                         ( Select Sum(Amount) from SeatManagement where BookingType = 1 and VehicleTripRegistrationId = seatm.VehicleTripRegistrationId) as AdvancedBookingSales,
					                         ( Select Sum(Amount) from SeatManagement where BookingType = 2 and VehicleTripRegistrationId = seatm.VehicleTripRegistrationId) as OnlineBookingSales
				
			                        from SeatManagement seatm join 
					                        VehicleTripRegistration vtr on seatm.VehicleTripRegistrationId = vtr.Id
					
					                        where (vtr.PhysicalBusRegistrationNumber is not null) and 
					                        vtr.DepartureDate between @StartDate and @EndDate
                        END
GO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
