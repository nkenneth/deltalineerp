﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class djustfarecalendar : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FareCalendar_Route_RouteId",
                table: "FareCalendar");

            migrationBuilder.AlterColumn<int>(
                name: "RouteId",
                table: "FareCalendar",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "FareAdjustmentType",
                table: "FareCalendar",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TerminalId",
                table: "FareCalendar",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VehicleModelId",
                table: "FareCalendar",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FareCalendar_TerminalId",
                table: "FareCalendar",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_FareCalendar_VehicleModelId",
                table: "FareCalendar",
                column: "VehicleModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_FareCalendar_Route_RouteId",
                table: "FareCalendar",
                column: "RouteId",
                principalTable: "Route",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FareCalendar_Terminal_TerminalId",
                table: "FareCalendar",
                column: "TerminalId",
                principalTable: "Terminal",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FareCalendar_VehicleModel_VehicleModelId",
                table: "FareCalendar",
                column: "VehicleModelId",
                principalTable: "VehicleModel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FareCalendar_Route_RouteId",
                table: "FareCalendar");

            migrationBuilder.DropForeignKey(
                name: "FK_FareCalendar_Terminal_TerminalId",
                table: "FareCalendar");

            migrationBuilder.DropForeignKey(
                name: "FK_FareCalendar_VehicleModel_VehicleModelId",
                table: "FareCalendar");

            migrationBuilder.DropIndex(
                name: "IX_FareCalendar_TerminalId",
                table: "FareCalendar");

            migrationBuilder.DropIndex(
                name: "IX_FareCalendar_VehicleModelId",
                table: "FareCalendar");

            migrationBuilder.DropColumn(
                name: "FareAdjustmentType",
                table: "FareCalendar");

            migrationBuilder.DropColumn(
                name: "TerminalId",
                table: "FareCalendar");

            migrationBuilder.DropColumn(
                name: "VehicleModelId",
                table: "FareCalendar");

            migrationBuilder.AlterColumn<int>(
                name: "RouteId",
                table: "FareCalendar",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_FareCalendar_Route_RouteId",
                table: "FareCalendar",
                column: "RouteId",
                principalTable: "Route",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
