﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class Sp_JourneyChartsByState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"CREATE OR ALTER PROCEDURE Sp_JourneyChartsByState
(
   @StartDate DateTime, @EndDate DateTime, @StateId int
)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON



    -- Insert statements for procedure here

	select Distinct Name, Id, TotalOutgoingCount, TotalIncomingCount, TotalOutgoingBlownCount, TotalIncomingBlownCount,
       (TotalIncomingCount + TotalIncomingBlownCount) as ExpectedStartupVehicles
	   from
   (select stateTerminal.Name, stateTerminal.Id,
(select count(*)  from JourneyManagement jm join VehicletripRegistration vtr
			on jm.VehicleTripRegistrationId = vtr.Id
			join Trip tr
			on tr.Id = vtr.TripId
			join Route rt
			on rt.Id = tr.RouteId
			join Terminal term
			on term.Id = rt.DepartureTerminalId
			join State sta
			on sta.Id = term.StateId
where vtr.DepartureDate between @StartDate and @EndDate and
 term.Id = stateTerminal.Id 
and (jm.JourneyStatus = 2 or jm.JourneyStatus = 1) and (jm.JourneyType = 0) 
) TotalOutgoingCount, 
(select count(*)  from JourneyManagement jm join VehicletripRegistration vtr
			on jm.VehicleTripRegistrationId = vtr.Id
			join Trip tr
			on tr.Id = vtr.TripId
			join Route rt
			on rt.Id = tr.RouteId
			join Terminal term
			on term.Id = rt.DestinationTerminalId
			join State sta
			on sta.Id = term.StateId
where vtr.DepartureDate between @StartDate and @EndDate and 
 term.Id = stateTerminal.Id  and
(jm.JourneyStatus = 2 or jm.JourneyStatus = 1) 
)
 TotalIncomingCount,
 (select count(*)  from JourneyManagement jm join VehicletripRegistration vtr
			on jm.VehicleTripRegistrationId = vtr.Id
			join Trip tr
			on tr.Id = vtr.TripId
			join Route rt
			on rt.Id = tr.RouteId
			join Terminal term
			on term.Id = rt.DepartureTerminalId
			join State sta
			on sta.Id = term.StateId
where vtr.DepartureDate between @StartDate and @EndDate and
 term.Id = stateTerminal.Id 
and (jm.JourneyStatus = 2 or jm.JourneyStatus = 0) 
)
 TotalOutgoingBlownCount,
 
 (select count(*)  from JourneyManagement jm join VehicletripRegistration vtr
			on jm.VehicleTripRegistrationId = vtr.Id
			join Trip tr
			on tr.Id = vtr.TripId
			join Route rt
			on rt.Id = tr.RouteId
			join Terminal term
			on term.Id = rt.DestinationTerminalId
			join State sta
			on sta.Id = term.StateId
where 
vtr.DepartureDate between @StartDate and @EndDate and
 term.Id = stateTerminal.Id 
and (jm.JourneyStatus = 2 or jm.JourneyStatus = 0) 
)
 TotalIncomingBlownCount
from 
Terminal stateTerminal


where StateId = @StateId)
 as SelectCount
 where(TotalOutgoingCount > 0) or(TotalIncomingCount > 0) or(TotalIncomingBlownCount > 0) or(TotalOutgoingBlownCount > 0)


END
GO");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
