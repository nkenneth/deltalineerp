﻿using LME.Core.Entities.Common;
using System;

namespace LME.Core.Entities
{
    public class GeneralLedger:FullAuditedEntity
    {
        public string Description { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal Amount { get; set; }
        public int TransactionTypeId { get; set; }
        public int TransactionSourceId { get; set; }
    }
}