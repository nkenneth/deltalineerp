﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class AddColtoSeatMgttbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ExpiredDate",
                table: "SeatManagement",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsGhanaRoute",
                table: "SeatManagement",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "IssuedDate",
                table: "SeatManagement",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PassportId",
                table: "SeatManagement",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PassportType",
                table: "SeatManagement",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PlaceOfIssue",
                table: "SeatManagement",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExpiredDate",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "IsGhanaRoute",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "IssuedDate",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "PassportId",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "PassportType",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "PlaceOfIssue",
                table: "SeatManagement");
        }
    }
}
