﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class vehiclemakeRequired : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleModel_VehicleMake_VehicleMakeId",
                table: "VehicleModel");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleMakeId",
                table: "VehicleModel",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleModel_VehicleMake_VehicleMakeId",
                table: "VehicleModel",
                column: "VehicleMakeId",
                principalTable: "VehicleMake",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleModel_VehicleMake_VehicleMakeId",
                table: "VehicleModel");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleMakeId",
                table: "VehicleModel",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleModel_VehicleMake_VehicleMakeId",
                table: "VehicleModel",
                column: "VehicleMakeId",
                principalTable: "VehicleMake",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
