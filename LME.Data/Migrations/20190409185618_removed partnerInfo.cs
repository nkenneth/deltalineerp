﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class removedpartnerInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Partner_PartnerId",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_Partner_PartnerId",
                table: "Vehicle");

            migrationBuilder.DropTable(
                name: "PartnerEnquiry");

            migrationBuilder.DropTable(
                name: "PartnersWalletTransaction");

            migrationBuilder.DropTable(
                name: "PartnersWallet");

            migrationBuilder.DropTable(
                name: "Partner");

            migrationBuilder.DropTable(
                name: "PartnerApplication");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_PartnerId",
                table: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Employee_PartnerId",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "PartnerId",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "PartnerId",
                table: "Employee");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PartnerId",
                table: "Vehicle",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PartnerId",
                table: "Employee",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PartnerApplication",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccountNumber = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    ApproverId = table.Column<int>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    CompanyRcNumber = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    IdentificationNumber = table.Column<string>(nullable: true),
                    IdentificationTypeId = table.Column<int>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsRegistered = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    PartnerApplicationStatus = table.Column<int>(nullable: false),
                    PartnerType = table.Column<int>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: true),
                    TellAboutYou = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartnerApplication", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartnerApplication_Employee_ApproverId",
                        column: x => x.ApproverId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PartnerApplication_IdentificationType_IdentificationTypeId",
                        column: x => x.IdentificationTypeId,
                        principalTable: "IdentificationType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PartnerEnquiry",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BusinessAddress = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    CompanyRcNumber = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    InspectionLocation = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NumberOfVehicles = table.Column<string>(nullable: true),
                    PartnershipEnquiryStatus = table.Column<int>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    VehicleSpec = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartnerEnquiry", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Partner",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccountNumber = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Bank = table.Column<int>(nullable: false),
                    BankName = table.Column<string>(nullable: true),
                    Code = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    OptionalPhoneNumber = table.Column<string>(nullable: true),
                    PartnerApplicationId = table.Column<int>(nullable: false),
                    PartnerType = table.Column<int>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: true),
                    WalletId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Partner", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Partner_PartnerApplication_PartnerApplicationId",
                        column: x => x.PartnerApplicationId,
                        principalTable: "PartnerApplication",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Partner_Wallet_WalletId",
                        column: x => x.WalletId,
                        principalTable: "Wallet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PartnersWallet",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Balance = table.Column<decimal>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    OperationalBalance = table.Column<decimal>(nullable: false),
                    PartnerCode = table.Column<Guid>(nullable: false),
                    PartnerId = table.Column<int>(nullable: true),
                    WalletNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartnersWallet", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartnersWallet_Partner_PartnerId",
                        column: x => x.PartnerId,
                        principalTable: "Partner",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PartnersWalletTransaction",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    CreatededBy = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    PartnerId = table.Column<int>(nullable: false),
                    PartnersWalletId = table.Column<int>(nullable: true),
                    SourceId = table.Column<string>(nullable: true),
                    TransactionType = table.Column<int>(nullable: false),
                    WalletId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartnersWalletTransaction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartnersWalletTransaction_PartnersWallet_PartnersWalletId",
                        column: x => x.PartnersWalletId,
                        principalTable: "PartnersWallet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PartnersWalletTransaction_PartnersWallet_WalletId",
                        column: x => x.WalletId,
                        principalTable: "PartnersWallet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_PartnerId",
                table: "Vehicle",
                column: "PartnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_PartnerId",
                table: "Employee",
                column: "PartnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Partner_PartnerApplicationId",
                table: "Partner",
                column: "PartnerApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_Partner_WalletId",
                table: "Partner",
                column: "WalletId");

            migrationBuilder.CreateIndex(
                name: "IX_PartnerApplication_ApproverId",
                table: "PartnerApplication",
                column: "ApproverId");

            migrationBuilder.CreateIndex(
                name: "IX_PartnerApplication_IdentificationTypeId",
                table: "PartnerApplication",
                column: "IdentificationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PartnersWallet_PartnerId",
                table: "PartnersWallet",
                column: "PartnerId");

            migrationBuilder.CreateIndex(
                name: "IX_PartnersWallet_WalletNumber",
                table: "PartnersWallet",
                column: "WalletNumber",
                unique: true,
                filter: "[WalletNumber] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_PartnersWalletTransaction_PartnersWalletId",
                table: "PartnersWalletTransaction",
                column: "PartnersWalletId");

            migrationBuilder.CreateIndex(
                name: "IX_PartnersWalletTransaction_WalletId",
                table: "PartnersWalletTransaction",
                column: "WalletId");

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Partner_PartnerId",
                table: "Employee",
                column: "PartnerId",
                principalTable: "Partner",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_Partner_PartnerId",
                table: "Vehicle",
                column: "PartnerId",
                principalTable: "Partner",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
