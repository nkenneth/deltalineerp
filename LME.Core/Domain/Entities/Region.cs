﻿using LME.Core.Entities.Common;

namespace LME.Core.Entities
{
    public class Region : FullAuditedEntity
    {
        public string Name { get; set; }
    }
}