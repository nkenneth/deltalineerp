﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace LME.Data.efCore.Mapping
{
    class BankPaymentConfig : IEntityTypeConfiguration<BankPayment>
    {
        public void Configure(EntityTypeBuilder<BankPayment> builder)
        {
            builder.ToTable(nameof(BankPayment));
        }
    }
}