﻿using IPagedList;
using LME.Business.Services;
using LME.Core.Domain.DataTransferObjects;
using LME.WebAPI.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LME.WebAPI.Controllers
{
    [Authorize]
    public class PatrolReportController : BaseController
    {
        private readonly IVehicleService _vehicleService;
        private readonly IMtuReports _mtuReportService;

        public PatrolReportController(IVehicleService vehicleService, IMtuReports mtuReportService)
        {
            _vehicleService = vehicleService;
            _mtuReportService = mtuReportService;
        }

        [HttpPost]
        [Route("Add")]
        public async Task<ServiceResponse<bool>> AddVehicle(PatrolReportModelDTO mtuReport)
        {
            mtuReport.Email = CurrentUser.UserName;
            //mtuReport.FullName = 
            return await HandleApiOperationAsync(async () => {
                await _mtuReportService.AddReport(mtuReport);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPost]
        [Route("GetAllReport")]
        [Route("GetAllReport/{pageNumber}/{pageSize}")]
        [Route("GetAllReport/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<PatrolReportModelDTO>>> GetAllReport(DateModel search,
            int pageNumber = 1,
            int pageSize = WebConstants.DefaultPageSize,
            string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                IPagedList<PatrolReportModelDTO> vehicles;

                vehicles =  await _mtuReportService.GetAllReport(search, pageNumber, pageSize, query);

                return new ServiceResponse<IPagedList<PatrolReportModelDTO>>
                {
                    Object = vehicles
                };
            });
        }
        [HttpGet]
        [Route("Get/{id}")]
        public async Task<ServiceResponse<PatrolReportModelDTO>> GetReportById(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var vehicles = await _mtuReportService.GetReportById(id);

                return new ServiceResponse<PatrolReportModelDTO>
                {
                    Object = vehicles
                };
            });
        }
    }
}