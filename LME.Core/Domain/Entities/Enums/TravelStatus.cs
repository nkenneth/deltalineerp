﻿namespace LME.Core.Entities.Enums
{
    public enum TravelStatus
    {
        Pending,
        Travelled,
        NoShow,
        Rescheduled
    }
}