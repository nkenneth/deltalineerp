﻿using Microsoft.Extensions.Configuration;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LME.Business.Services
{
    public interface ITOTPService {
        Task<string> GetTerminalCode(int terminalId, bool doTimeAdjustment = false);
        Task<bool> ValidateTerminalCode(string code, int terminalId);
        Task<string> GetRegionCode(int regionId, bool doTimeAdjustment = false);
        Task<bool> ValidateRegionCode(string code, int regionId);
        Task<string> GetForcedSwapCode(int regionId, bool doTimeAdjustment = false);
        Task<bool> ValidateForcedSwapCode(string code, int regionId);
        Task<string> GetTerminalTicketRemovalCode(int terminalId, bool doTimeAdjustment = false);
        Task<bool> ValidateTerminalTicketRemovalCode(string code, int terminalId);
        Task<string> GetTerminalAddRefCodeToClosedManifestCode(int terminalId, bool doTimeAdjustment = false);
        Task<bool> ValidateTerminalAddRefCodeToClosedManifestCode(string code, int terminalId);
    }

    public class TOTPService : ITOTPService
    {
        private TOTPGenerator _generator;
        private string _baseSecret = "XOV0SD890ALSKLDJVSD09BA0D80APPFALSDKFJADF";
        private readonly IConfiguration _configuration;
        public TOTPService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private void _Initialize()
        {
            _generator = new TOTPGenerator();
            _ReadSteps();
        }
        private void _ReadSteps()
        {
            var steps = _configuration.GetValue<string>("timeStepInSeconds");

            if (string.IsNullOrEmpty(steps))
            {
                _generator.Steps = 60;
            }
            else
            {
                _generator.Steps = Convert.ToInt32(steps);
            }
        }


        public async Task<string> GetRegionCode(int regionId, bool doTimeAdjustment = false)
        {
            _Initialize();

            string regionSecret = _baseSecret + regionId + "0000000000";
            string regionOTP = string.Empty;

            _generator.Secret = Encoding.UTF32.GetBytes(regionSecret);


            if (doTimeAdjustment == false)
                regionOTP = _generator.GetNextOTP();
            else
                regionOTP = _generator.GetLastOTP();



            return await Task.FromResult(regionOTP);
        }

        public async Task<bool> ValidateRegionCode(string code, int regionId)
        {
            _Initialize();

            if (string.IsNullOrEmpty(code))
                return false;


            if (code == await GetRegionCode(regionId))
            {
                return true;
            }
            else
            {
                // Do Time Adjusted Validation Before
                return code == await GetRegionCode(regionId, true);
            }

            // return false;
        }

        public async Task<string> GetTerminalTicketRemovalCode(int terminalId, bool doTimeAdjustment = false)
        {
            _Initialize();


            string terminalSecret = _baseSecret + terminalId + "0010110110";
            string terminalOTP = string.Empty;


            _generator.Secret = Encoding.UTF32.GetBytes(terminalSecret);

            if (doTimeAdjustment == false)
                terminalOTP = _generator.GetNextOTP();
            else
                terminalOTP = _generator.GetLastOTP();


            return await Task.FromResult(terminalOTP);
        }


        public async Task<bool> ValidateTerminalTicketRemovalCode(string code, int terminalId)
        {
            _Initialize();

            if (string.IsNullOrEmpty(code))
                return false;


            if (code == await GetTerminalTicketRemovalCode(terminalId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<string> GetTerminalAddRefCodeToClosedManifestCode(int terminalId, bool doTimeAdjustment = false)
        {
            _Initialize();


            string terminalSecret = _baseSecret + terminalId + "A83G*4s@uX";
            string terminalOTP = string.Empty;


            _generator.Secret = Encoding.UTF32.GetBytes(terminalSecret);

            if (doTimeAdjustment == false)
                terminalOTP = _generator.GetNextOTP();
            else
                terminalOTP = _generator.GetLastOTP();


            return await Task.FromResult(terminalOTP);
        }


        public async Task<bool> ValidateTerminalAddRefCodeToClosedManifestCode(string code, int terminalId)
        {
            _Initialize();

            if (string.IsNullOrEmpty(code))
                return false;


            if (code == await GetTerminalAddRefCodeToClosedManifestCode(terminalId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<string> GetTerminalCode(int terminalId, bool doTimeAdjustment = false)
        {
            _Initialize();


            string terminalSecret = _baseSecret + terminalId + "0000000000";
            string terminalOTP = string.Empty;


            _generator.Secret = Encoding.UTF32.GetBytes(terminalSecret);

            if (doTimeAdjustment == false)
                terminalOTP = _generator.GetNextOTP();
            else
                terminalOTP = _generator.GetLastOTP();


            return await Task.FromResult(terminalOTP);
        }

        public async Task<bool> ValidateTerminalCode(string code, int terminalId)
        {
            _Initialize();

            if (string.IsNullOrEmpty(code))
                return false;


            if (code == await GetTerminalCode(terminalId))
            {
                return true;
            }
            else
            {
                // Do time adjusted validation before
                return code == await GetTerminalCode(terminalId, true);
            }
            // return false;

        }

        public async Task<string> GetForcedSwapCode(int regionId, bool doTimeAdjustment = false)
        {
            _Initialize();

            string regionSecret = _baseSecret + regionId + "1010010001";
            string regionOTP = string.Empty;

            _generator.Secret = Encoding.UTF32.GetBytes(regionSecret);


            if (doTimeAdjustment == false)
                regionOTP = _generator.GetNextOTP();
            else
                regionOTP = _generator.GetLastOTP();



            return await Task.FromResult(regionOTP);
        }

        public async Task<bool> ValidateForcedSwapCode(string code, int regionId)
        {
            _Initialize();

            if (string.IsNullOrEmpty(code))
                return false;

            var currentSwapCode = await GetForcedSwapCode(regionId);
            if (code == currentSwapCode)
            {
                return true;
            }
            else if (code == await GetForcedSwapCode(regionId, true))
            {
                return true;
            }
            else
            {
                return false;
                // Do Time Adjusted Validation Before
                //return code == await GetForcedSwapCode(regionId, true);
            }

            // return false;
        }

    }

    public class TOTPGenerator
    {

        public const int SECRET_LENGTH = 20;
        private const string
        MSG_SECRETLENGTH = "Minimum secret length is 12",
        MSG_COUNTER_MINVALUE = "Minimum counter value is 1";

        public TOTPGenerator()
        {
        }

        private static int[] dd = new int[10] { 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 };

        private byte[] secretKey = new byte[SECRET_LENGTH]
        {
    0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
    0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F, 0x40, 0x41, 0x42, 0x43
        };

        private double counter = 0x0000000000000001;

        private static int checksum(int Code_Digits)
        {
            int d1 = (Code_Digits / 1000000) % 10;
            int d2 = (Code_Digits / 100000) % 10;
            int d3 = (Code_Digits / 10000) % 10;
            int d4 = (Code_Digits / 1000) % 10;
            int d5 = (Code_Digits / 100) % 10;
            int d6 = (Code_Digits / 10) % 10;
            int d7 = Code_Digits % 10;
            return (10 - ((dd[d1] + d2 + dd[d3] + d4 + dd[d5] + d6 + dd[d7]) % 10)) % 10;
        }

        /// <summary>
        /// Formats the OTP. This is the OTP algorithm.
        /// </summary>
        /// <param name="hmac">HMAC value</param>
        /// <returns>8 digits OTP</returns>
        private static string FormatOTP(byte[] hmac)
        {
            int offset = hmac[19] & 0xf;
            int bin_code = (hmac[offset] & 0x7f) << 24
                | (hmac[offset + 1] & 0xff) << 16
                | (hmac[offset + 2] & 0xff) << 8
                | (hmac[offset + 3] & 0xff);
            int Code_Digits = bin_code % 10000000;
            int csum = checksum(Code_Digits);
            int OTP = Code_Digits * 10 + csum;

            return string.Format("{0:d08}", OTP);
        }

        public byte[] CounterArray
        {
            get
            {
                return BitConverter.GetBytes(counter);
            }

            set
            {
                counter = BitConverter.ToUInt64(value, 0);
            }
        }

        /// <summary>
        /// Sets the OTP secret
        /// </summary>
        public byte[] Secret
        {
            set
            {
                if (value.Length < SECRET_LENGTH)
                {
                    throw new Exception(MSG_SECRETLENGTH);
                }

                secretKey = value;
            }
        }

        /// <summary>
        /// Gets the current OTP value
        /// </summary>
        /// <returns>8 digits OTP</returns>
        public string GetCurrentOTP()
        {
            HMACSHA1 hmacSha1 = new HMACSHA1(secretKey);

            hmacSha1.Initialize();

            byte[] hmac_result = hmacSha1.ComputeHash(CounterArray);

            return FormatOTP(hmac_result);
        }


        /// <summary>
        /// Gets OTP For the Last Time Window
        /// </summary>
        /// <returns></returns>
        public string GetLastOTP()
        {
            counter = Math.Floor(DateTime.UtcNow.AddSeconds(-_steps).Subtract(initialTime).TotalSeconds / (_steps));
            return GetCurrentOTP();
        }
        /// <summary>
        /// Gets the next OTP value
        /// </summary>
        /// <returns>8 digits OTP</returns>
        public string GetNextOTP()
        {
            // increment the counter
            // ++counter;

            counter = Math.Floor(DateTime.UtcNow.Subtract(initialTime).TotalMilliseconds / (_steps * 1000));

            return GetCurrentOTP();
        }


        public int Steps
        {
            get
            {
                return _steps;
            }
            set
            {
                _steps = value;
            }
        }
        private int _steps = 1200;
        private DateTime initialTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        /// <summary>
        /// Gets/sets the counter value
        /// </summary>
        public double Counter
        {
            get
            {
                return counter;
            }

            set
            {
                counter = value;
            }
        }
    }
}
