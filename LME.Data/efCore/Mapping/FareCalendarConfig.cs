﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class FareCalendarConfig : IEntityTypeConfiguration<FareCalendar>
    {
        public void Configure(EntityTypeBuilder<FareCalendar> builder)
        {
            builder.ToTable(nameof(FareCalendar));

        }
    }
}