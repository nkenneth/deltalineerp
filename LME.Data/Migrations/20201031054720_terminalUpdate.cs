﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class terminalUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TicketRemovalOtp",
                table: "Terminal",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "TicketRemovalOtpIsUsed",
                table: "Terminal",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TicketRemovalOtp",
                table: "Terminal");

            migrationBuilder.DropColumn(
                name: "TicketRemovalOtpIsUsed",
                table: "Terminal");
        }
    }
}
