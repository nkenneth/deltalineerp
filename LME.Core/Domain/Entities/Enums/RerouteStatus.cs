﻿namespace LME.Core.Entities.Enums
{
    public enum RerouteStatus
    {
        NotReroute,
        Pending,
        PayAtTerminal,
        Accepted,
        Declined
    }
}