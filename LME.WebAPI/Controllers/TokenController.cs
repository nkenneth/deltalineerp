﻿using IdentityModel;
using LME.Business.Services;
using LME.Core.DataTransferObjects;
using LME.Core.Entities;
using LME.WebAPI.Infrastructure.Services;
using LME.WebAPI.Utils;
using LME.WebAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace LME.WebAPI.Controllers
{
    public class TokenController : BaseController
    {
        private readonly IUserService _userSvc;
        private readonly IRoleService _roleSvc;
        private readonly ITokenService _tokenSvc;
        private readonly ITOTPService _totpSvc;
        private readonly IServiceHelper _serviceHelper;
        private readonly IEmployeeService _empSvc;

        public TokenController(IUserService usersvc,
            ITokenService tokenSvc, IRoleService rolesvc,
            ITOTPService totpSvc, IServiceHelper serviceHelper,
            IEmployeeService empSvc)
        {
            _userSvc = usersvc;
            _tokenSvc = tokenSvc;
            _roleSvc = rolesvc;
            _totpSvc = totpSvc;
            _serviceHelper = serviceHelper;
            _empSvc = empSvc;
        }

        [HttpPost]
        public async Task<IServiceResponse<TokenDTO>> Index([FromBody] LoginModel model)
        {
            return await HandleApiOperationAsync(async () =>
            {

                var response = new ServiceResponse<TokenDTO>();

                var user = await _userSvc.FindByNameAsync(model.Username)
                        ?? await _userSvc.FindByEmailAsync(model.Username);

                if (!user.IsNull() && await _userSvc.CheckPasswordAsync(user, model.Password))
                {

                    if (!user.IsDefaultAccount())
                    {

                        if (!user.IsConfirmed())
                        {

                            response.Code = HttpStatusCode.BadRequest.GetStatusCodeValue();
                            response.ShortDescription = "Account not active. Please activate your acccount to continue.";
                            return response;
                        }

                        if (user.AccountLocked())
                        {
                            response.Code = HttpStatusCode.BadRequest.GetStatusCodeValue();
                            response.ShortDescription = "Account locked. Please contact the system administrator.";
                            return response;
                        }

                    }

                    var userClaims = user.UserToClaims();
                    //userClaims.AddRange(await RoleClaims(user));

                    var token = _tokenSvc.GenerateAccessTokenFromClaims(userClaims.ToArray());

                    user.RefreshToken = token.RefreshToken;
                    await _userSvc.UpdateAsync(user);

                    response.Object = token;
                }

                else
                {
                    response.Code = HttpStatusCode.BadRequest.GetStatusCodeValue();
                    response.ShortDescription = "Invalid credentials supplied.";
                }

                return response;
            });
        }

        [HttpPost]
        [Route("RefreshToken")]
        public async Task<IServiceResponse<TokenDTO>> Refresh(RefreshTokenModel model)
        {
            return await HandleApiOperationAsync(async () =>
            {

                var response = new ServiceResponse<TokenDTO>();

                var principal = _tokenSvc.GetPrincipalFromExpiredToken(model.AccessToken);
                if (principal != null)
                {
                    var username = principal.FindFirst(JwtClaimTypes.Name).Value;

                    var user = await _userSvc.FindByNameAsync(username);

                    if (user is null || user.RefreshToken != model.RefreshToken)
                    {
                        response.Code = HttpStatusCode.BadRequest.GetStatusCodeValue();
                        response.ShortDescription = "Invalid token supplied.";
                        return response;
                    }

                    var userClaims = user.UserToClaims();
                    //userClaims.AddRange(await RoleClaims(user));

                    var token = _tokenSvc.GenerateAccessTokenFromClaims(userClaims.ToArray());

                    user.RefreshToken = token.RefreshToken;
                    await _userSvc.UpdateAsync(user);

                    response.Object = token;

                    return response;
                }

                response.Code = HttpStatusCode.BadRequest.GetStatusCodeValue();
                response.ShortDescription = "User is invalid.";
                return response;
            });
        }

        [HttpPost]
        [Route("ValidateTerminalTicketRemovalToken")]
        public async Task<IServiceResponse<bool>> ValidateTerminalTicketRemovalToken(TerminalTokenDto tokenDto)
        {

            return await HandleApiOperationAsync(async () =>
            {

                if (tokenDto.TerminalId == 0)
                {
                    var userEmail = _serviceHelper.GetCurrentUserEmail();
                    var terminalId = await _empSvc.GetAssignedTerminal(userEmail);

                    tokenDto.TerminalId = terminalId.GetValueOrDefault();
                }

                var isValid = await _totpSvc.ValidateTerminalTicketRemovalCode(tokenDto.TokenToValidate, tokenDto.TerminalId);

                return new ServiceResponse<bool>(isValid)
                {
                    ShortDescription = isValid ? "Success" : "Token Mismatch",
                };
            });

        }

        [HttpPost]
        [Route("ValidateTerminalAddRefcodeToClosedManifestToken")]
        public async Task<IServiceResponse<bool>> ValidateTerminalRefCodeToClosedManifestToken(TerminalTokenDto tokenDto)
        {

            return await HandleApiOperationAsync(async () =>
            {

                if (tokenDto.TerminalId == 0)
                {
                    var userEmail = _serviceHelper.GetCurrentUserEmail();
                    var terminalId = await _empSvc.GetAssignedTerminal(userEmail);

                    tokenDto.TerminalId = terminalId.GetValueOrDefault();
                }

                var isValid = await _totpSvc.ValidateTerminalAddRefCodeToClosedManifestCode(tokenDto.TokenToValidate, tokenDto.TerminalId);

                return new ServiceResponse<bool>(isValid)
                {
                    ShortDescription = isValid ? "Success" : "Token Mismatch",
                };
            });
        }
    }
}