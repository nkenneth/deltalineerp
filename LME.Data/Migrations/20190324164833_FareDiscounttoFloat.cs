﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class FareDiscounttoFloat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "PayStackWebhookResponse",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "PayStackWebhookResponse",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "PayStackWebhookResponse",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "PayStackWebhookResponse",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "PayStackWebhookResponse",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "PayStackWebhookResponse",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "PayStackWebhookResponse",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "PayStackWebhookResponse",
                nullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "ChildrenDiscountPercentage",
                table: "Fare",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "PayStackWebhookResponse");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "PayStackWebhookResponse");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "PayStackWebhookResponse");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "PayStackWebhookResponse");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "PayStackWebhookResponse");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "PayStackWebhookResponse");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "PayStackWebhookResponse");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "PayStackWebhookResponse");

            migrationBuilder.AlterColumn<int>(
                name: "ChildrenDiscountPercentage",
                table: "Fare",
                nullable: true,
                oldClrType: typeof(float),
                oldNullable: true);
        }
    }
}
