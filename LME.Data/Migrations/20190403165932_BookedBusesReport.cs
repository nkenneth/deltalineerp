﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class BookedBusesReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sql = @"CREATE  PROCEDURE [dbo].[Sp_BookingReport]
	                        -- Add the parameters for the stored procedure here
		                        @TerminalId int,
		                        @BookingType int,
		                        @StartDate DateTime,
		                        @EndDate DateTime, 
								@PageIndex int, 
								@PageSize int
                        AS
                        BEGIN
	                        -- SET NOCOUNT ON added to prevent extra result sets from
	                        -- interfering with SELECT statements.
	                        SET NOCOUNT ON;

                            -- Insert statements for procedure here
	                        select seats.SeatNumber, seats.BookingReferenceCode, seats.PhoneNumber as CustomerPhoneNumber,
								seats.NextOfKinName as NextOfKinName, seats.Amount, seats.Discount, 
								(seats.Amount - seats.Discount) as DiscountedAmount, 
								(usr.FirstName + ' ' + usr.LastName) as EmployeeName, usr.Email as EmployeeEmail,
								term.Name as TerminalName,  [TotalCount]= COUNT(*) OVER()
								
								from
	                        Seatmanagement seats 
                            join AspNetUsers usr
	                        on seats.CreatedBy = usr.Id
	                        join Employee emp 
	                        on emp.UserId = usr.Id
	                        join Terminal term 
	                        on term.Id = emp.TerminalId
	                        where 
		                        (seats.CreationTime between @StartDate and @EndDate)
		                         and
		                        (BookingType = 1)
		                        and 
		                        (seats.isdeleted = 0) and
	                           (@TerminalId is null or term.Id = @TerminalId) and
	                           (@bookingType is null or seats.BookingType = @BookingType)
							   ORDER BY seats.CreationTime desc
							   OFFSET (@PageIndex - 1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY

                        END
";
            migrationBuilder.Sql(sql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
