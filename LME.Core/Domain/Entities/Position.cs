﻿using LME.Core.Entities.Common;

namespace LME.Core.Entities
{
    public class Position : FullAuditedEntity
    {
        public string Name { get; set; }

    }
}