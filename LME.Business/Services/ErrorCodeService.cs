﻿using LME.Core.Entities;
using LME.Data.Repository;
using System.Threading.Tasks;

namespace LME.Business.Services
{
    public interface IErrorCodeService
    {
        Task<ErrorCode> GetErrorByCodeAsync(string errorCode);
    }

    public class ErrorCodeService : IErrorCodeService
    {
        readonly IRepository<ErrorCode> _repository;

        public ErrorCodeService(IRepository<ErrorCode> repository)
        {
            _repository = repository;
        }

        public Task<ErrorCode> GetErrorByCodeAsync(string errorCode)
        {
            return _repository.FirstOrDefaultAsync(e => e.Code.ToLower() == errorCode.ToLower());
        }
    }
}