﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class AddColtoBookingtbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ExpiredDate",
                table: "Booking",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsGhanaRoute",
                table: "Booking",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "IssuedDate",
                table: "Booking",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PassportId",
                table: "Booking",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PassportType",
                table: "Booking",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PlaceOfIssue",
                table: "Booking",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExpiredDate",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "IsGhanaRoute",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "IssuedDate",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "PassportId",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "PassportType",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "PlaceOfIssue",
                table: "Booking");
        }
    }
}
