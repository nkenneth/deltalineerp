﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class AlterBookingReports4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sql = @"ALTER  PROCEDURE  [dbo].[Sp_BookingReport]
	                        -- Add the parameters for the stored procedure here
		                        @TerminalId int,
								@keyword nvarchar(50),
		                        @BookingType int,
								@BookingStatus int,
								@CreatedBy nvarchar(50),
								@ReferenceCode nvarchar(50),
		                        @StartDate DateTime,
		                        @EndDate DateTime, 
								@PageIndex int, 
								@PageSize int
                        AS
                        BEGIN
	                        -- SET NOCOUNT ON added to prevent extra result sets from
	                        -- interfering with SELECT statements.
	                        SET NOCOUNT ON;

                            -- Insert statements for procedure here
	                        select seats.Id, seats.SeatNumber, seats.BookingReferenceCode, seats.PhoneNumber as CustomerPhoneNumber,
								seats.NextOfKinName as NextOfKinName, seats.Amount, seats.Discount,  seats.BookingStatus, seats.BookingType,
								(seats.Amount - seats.Discount) as DiscountedAmount, 
								ISNULL(usr.FirstName, '') + ' ' + ISNULL(usr.LastName, '') [EmployeeName], usr.Email as EmployeeEmail
								,
								depterm.Name as TerminalName, destTerm.Name as Destination, 
								
								 [TotalCount]= COUNT(*) OVER(),
								 [TotalSales] = Sum(seats.Amount) over(), [TotalDiscountedSales] = sum(seats.Amount - seats.Discount) over ()
								from
	                        Seatmanagement seats 
                            join AspNetUsers usr
	                        on seats.CreatedBy = usr.UserName
	                        join Employee emp 
	                        on emp.UserId = usr.Id
							join Route rt 
							on rt.Id = seats.RouteId
	                        join Terminal depterm 
	                        on depterm.Id = rt.DepartureTerminalId
							join Terminal destTerm
							on destTerm.Id = rt.DestinationTerminalId
	                        where 
		                        (seats.CreationTime between @StartDate and @EndDate)
		                         and
		                        (seats.isdeleted = 0) and
	                           (@TerminalId is null or depterm.Id = @TerminalId) and
	                           (@bookingType is null or seats.BookingType = @BookingType) and
	                           (@BookingStatus is null or seats.BookingStatus = @BookingStatus) and

							   (@keyword is null or (seats.FullName like '%' + @keyword +'%' or 
													 seats.BookingReferenceCode like '%' + @keyword +'%' or 
													 seats.PhoneNumber like '%' + @keyword +'%' or
													 seats.BookingReferenceCode like '%' + @keyword +'%'))
							   and
							   (@CreatedBy is null or usr.Email  like  '%' + @CreatedBy +'%')
							   ORDER BY seats.CreationTime desc
							   OFFSET (@PageIndex - 1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY

                        END";
            migrationBuilder.Sql(sql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
