﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class EmployeeRouteConfig : IEntityTypeConfiguration<EmployeeRoute>
    {
        public void Configure(EntityTypeBuilder<EmployeeRoute> builder)
        {
            builder.HasKey(x => x.Id);
            builder.ToTable(nameof(EmployeeRoute));

        }
    }
}