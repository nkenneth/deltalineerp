﻿using LME.Core.Entities.Common;
using LME.Core.Entities.Enums;
using System;

namespace LME.Core.Entities
{
    public class VehicleMileage : FullAuditedEntity<Guid>
    {
        public string VehicleRegistrationNumber { get; set; }
        public ServiceLevelType ServiceLevel { get; set; }

        public int CurrentMileage { get; set; }
        public DateTime? LastServiceDate { get; set; }

        /// <summary>
        /// Date the vehicle mileage reached the current service level
        /// </summary>
        public DateTime? DateDue { get; set; }
        public bool IsDue { get; set; }
        public bool IsDeactivated { get; set; }
        
        public NotificationLevel NotificationLevel { get; set; }
    }
}
