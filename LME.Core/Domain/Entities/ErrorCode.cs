﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LME.Core.Entities
{
   public class ErrorCode:Entity
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Description { get; set; }
    }
}
