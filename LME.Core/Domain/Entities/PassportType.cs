﻿namespace LME.Core.Entities
{
    public class PassportType:Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int RouteId { get; set; }
        public decimal AddOnFare { get; set; }
    }
}