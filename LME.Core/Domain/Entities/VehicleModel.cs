﻿using LME.Core.Entities.Common;

namespace LME.Core.Entities
{
    public class VehicleModel : FullAuditedEntity
    {
        public string Name { get; set; }
        public int NumberOfSeats { get; set; }
        public string VehicleModelTypeCode { get; set; }
        public int VehicleMakeId { get; set; }
        public virtual VehicleMake VehicleMake { get; set; }
    }
}