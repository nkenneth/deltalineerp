﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class subroute : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SubRouteNameId",
                table: "SubRoute",
                newName: "NameId");

            migrationBuilder.RenameColumn(
                name: "SubRouteName",
                table: "SubRoute",
                newName: "Name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NameId",
                table: "SubRoute",
                newName: "SubRouteNameId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "SubRoute",
                newName: "SubRouteName");
        }
    }
}
