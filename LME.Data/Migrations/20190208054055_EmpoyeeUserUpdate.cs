﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class EmpoyeeUserUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EBM",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "DateJoined",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "EmployeeCode",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "EmployeePhoto",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "Gender",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "MiddleName",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "NextOfKin",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "NextOfKinPhone",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "AccountIsConfirmed",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Otp",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OtpIsUsed",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OtpNoOfTimeUsed",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "TicketRemovalOtpIsUsed",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "OriginalCaptainCode",
                table: "VehicleTripRegistration",
                newName: "OriginalDriverCode");

            migrationBuilder.RenameColumn(
                name: "CaptainCode",
                table: "VehicleTripRegistration",
                newName: "DriverCode");

            migrationBuilder.RenameColumn(
                name: "VehicleModelName",
                table: "VehicleModel",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "TicketRemovalOtp",
                table: "AspNetUsers",
                newName: "AccountConfirmationCode");

            migrationBuilder.RenameColumn(
                name: "OTPLastUsedDate",
                table: "AspNetUsers",
                newName: "LastModificationTime");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "WalletTransaction",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "WalletTransaction",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "WalletTransaction",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "WalletTransaction",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "WalletTransaction",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "WalletTransaction",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "WalletTransaction",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "VehicleTripRegistration",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "VehicleTripRegistration",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "VehicleTripRegistration",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "VehicleTripRegistration",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "VehicleTripRegistration",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "VehicleTripRegistration",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "VehicleTripRegistration",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "VehiclePartPosition",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "VehiclePartPosition",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "VehiclePartPosition",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "VehiclePartPosition",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "VehiclePartPosition",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "VehiclePartPosition",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "VehiclePartPosition",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "VehicleMileage",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "VehicleMileage",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "VehicleMileage",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "VehicleMileage",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "VehicleMileage",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "VehicleMileage",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "VehicleMileage",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "TripSetting",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "TripSetting",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "TripSetting",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "TripSetting",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "TripSetting",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "TripSetting",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "TripSetting",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "TripAvailability",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "TripAvailability",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "TripAvailability",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "TripAvailability",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "TripAvailability",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "TripAvailability",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "TripAvailability",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Trip",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "Trip",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "Trip",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Trip",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Trip",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Trip",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "Trip",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsNew",
                table: "Terminal",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "SeatManagement",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "SeatManagement",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "SeatManagement",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "SeatManagement",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "SeatManagement",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "SeatManagement",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "SeatManagement",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "ManifestManagement",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "ManifestManagement",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "ManifestManagement",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "ManifestManagement",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "ManifestManagement",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "ManifestManagement",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "ManifestManagement",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "JourneyManagement",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "JourneyManagement",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "JourneyManagement",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "JourneyManagement",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "JourneyManagement",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "JourneyManagement",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "JourneyManagement",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Expense",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "Expense",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "Expense",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Expense",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Expense",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Expense",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "Expense",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "EmployeeRoute",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "EmployeeRoute",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "EmployeeRoute",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "EmployeeRoute",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "EmployeeRoute",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "EmployeeRoute",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "EmployeeRoute",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Employee",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "DriverAccount",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "DriverAccount",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "DriverAccount",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "DriverAccount",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "DriverAccount",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "DriverAccount",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "DriverAccount",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "CustomerCouponRegistration",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "CustomerCouponRegistration",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "CustomerCouponRegistration",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "CustomerCouponRegistration",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "CustomerCouponRegistration",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "CustomerCouponRegistration",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "CustomerCouponRegistration",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateJoined",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "AccountTransaction",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "AccountTransaction",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "AccountTransaction",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "AccountTransaction",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "AccountTransaction",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "AccountTransaction",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "AccountTransaction",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "AccountSummary",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "AccountSummary",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "AccountSummary",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "AccountSummary",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "AccountSummary",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "AccountSummary",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "AccountSummary",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Employee_UserId",
                table: "Employee",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_AspNetUsers_UserId",
                table: "Employee",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee_AspNetUsers_UserId",
                table: "Employee");

            migrationBuilder.DropIndex(
                name: "IX_Employee_UserId",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "WalletTransaction");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "WalletTransaction");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "WalletTransaction");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "WalletTransaction");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "WalletTransaction");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "WalletTransaction");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "WalletTransaction");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "VehicleTripRegistration");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "VehicleTripRegistration");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "VehicleTripRegistration");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "VehicleTripRegistration");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "VehicleTripRegistration");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "VehicleTripRegistration");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "VehicleTripRegistration");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "VehiclePartPosition");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "VehiclePartPosition");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "VehiclePartPosition");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "VehiclePartPosition");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "VehiclePartPosition");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "VehiclePartPosition");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "VehiclePartPosition");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "VehicleMileage");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "VehicleMileage");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "VehicleMileage");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "VehicleMileage");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "VehicleMileage");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "VehicleMileage");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "VehicleMileage");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "TripSetting");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "TripSetting");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "TripSetting");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "TripSetting");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "TripSetting");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "TripSetting");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "TripSetting");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "TripAvailability");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "TripAvailability");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "TripAvailability");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "TripAvailability");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "TripAvailability");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "TripAvailability");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "TripAvailability");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Trip");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Trip");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "Trip");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Trip");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Trip");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Trip");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "Trip");

            migrationBuilder.DropColumn(
                name: "IsNew",
                table: "Terminal");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "ManifestManagement");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "ManifestManagement");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "ManifestManagement");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "ManifestManagement");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "ManifestManagement");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "ManifestManagement");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "ManifestManagement");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "JourneyManagement");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "JourneyManagement");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "JourneyManagement");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "JourneyManagement");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "JourneyManagement");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "JourneyManagement");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "JourneyManagement");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Expense");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Expense");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "Expense");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Expense");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Expense");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Expense");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "Expense");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "EmployeeRoute");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "EmployeeRoute");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "EmployeeRoute");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "EmployeeRoute");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "EmployeeRoute");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "EmployeeRoute");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "EmployeeRoute");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "DriverAccount");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "DriverAccount");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "DriverAccount");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "DriverAccount");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "DriverAccount");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "DriverAccount");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "DriverAccount");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "CustomerCouponRegistration");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "CustomerCouponRegistration");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "CustomerCouponRegistration");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "CustomerCouponRegistration");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "CustomerCouponRegistration");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "CustomerCouponRegistration");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "CustomerCouponRegistration");

            migrationBuilder.DropColumn(
                name: "DateJoined",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "AccountTransaction");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "AccountTransaction");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "AccountTransaction");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "AccountTransaction");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "AccountTransaction");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "AccountTransaction");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "AccountTransaction");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "AccountSummary");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "AccountSummary");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "AccountSummary");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "AccountSummary");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "AccountSummary");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "AccountSummary");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "AccountSummary");

            migrationBuilder.RenameColumn(
                name: "OriginalDriverCode",
                table: "VehicleTripRegistration",
                newName: "OriginalCaptainCode");

            migrationBuilder.RenameColumn(
                name: "DriverCode",
                table: "VehicleTripRegistration",
                newName: "CaptainCode");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "VehicleModel",
                newName: "VehicleModelName");

            migrationBuilder.RenameColumn(
                name: "LastModificationTime",
                table: "AspNetUsers",
                newName: "OTPLastUsedDate");

            migrationBuilder.RenameColumn(
                name: "AccountConfirmationCode",
                table: "AspNetUsers",
                newName: "TicketRemovalOtp");

            migrationBuilder.AddColumn<string>(
                name: "EBM",
                table: "Vehicle",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateJoined",
                table: "Employee",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmployeeCode",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmployeePhoto",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Gender",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MiddleName",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NextOfKin",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NextOfKinPhone",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AccountIsConfirmed",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Otp",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "OtpIsUsed",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "OtpNoOfTimeUsed",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "TicketRemovalOtpIsUsed",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);
        }
    }
}
