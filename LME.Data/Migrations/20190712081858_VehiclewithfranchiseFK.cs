﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class VehiclewithfranchiseFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FranchizeId",
                table: "Vehicle",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_FranchizeId",
                table: "Vehicle",
                column: "FranchizeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_Franchize_FranchizeId",
                table: "Vehicle",
                column: "FranchizeId",
                principalTable: "Franchize",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_Franchize_FranchizeId",
                table: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_FranchizeId",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "FranchizeId",
                table: "Vehicle");
        }
    }
}
