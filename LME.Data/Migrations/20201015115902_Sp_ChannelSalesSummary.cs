﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class Sp_ChannelSalesSummary : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"Create PROCEDURE [dbo].[Sp_ChannelbookingSalesSummary] (@sDate NVARCHAR(20),
		@eDate NVARCHAR(20), @status int, @channel int, @terminalId int) 
AS 
  
  BEGIN 
  SET DATEFORMAT dmy;
   
    DECLARE @startDate DATE=GetDate(), @endDate DATE=GetDate(); ; 

    IF(Isdate(@sDate)=1) 
    SET @startDate=Cast (@sDate AS DATE); 
	print @startDate

    IF(Isdate(@eDate)=1) 
    SET @endDate=Cast (@eDate AS DATE); 
    
		SELECT  sm.BookingType, sum(sm.Amount) SalesAmmount,sum(sm.Amount - sm.Discount) DiscountedAmount, BookingType
    FROM     seatmanagement sm
			 
    WHERE    (@channel IS NULL OR @channel = sm.bookingtype) 
    AND      (@status IS NULL OR  @status =sm.BookingStatus)
	
    AND      sm.isdeleted=0 
    AND      ((@startDate IS NULL OR Cast (sm.creationtime AS DATE)>=@startDate )
			 AND
             (@endDate IS NULL OR CONVERT(DATE,sm.creationtime)<=@endDate))
	
	Group by sm.BookingType

  END");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
