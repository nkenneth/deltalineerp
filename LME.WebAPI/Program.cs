﻿using LME.WebAPI.Utils;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace LME.WebAPI
{
    public class Program
    {
        //private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        public static void Main(string[] args)
        {
            CreateWebHost(args)

                .ConfigureLog4net()
                .Build()
                .Run();
        }

        public static IWebHostBuilder CreateWebHost(string[] args) =>
           
            WebHost.CreateDefaultBuilder(args)
           // .ConfigureLogging(ConfigLogging)
                .UseStartup<Startup>();
        //static void ConfigLogging(ILoggingBuilder bldr)
        //{
        //    bldr.AddFilter(DbLoggerCategory.Database.Connection.Name, LogLevel.Information);
        //}
    }
}