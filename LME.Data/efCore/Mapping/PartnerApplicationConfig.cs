﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class PartnerApplicationConfig : IEntityTypeConfiguration<PartnerApplication>
    {
        public void Configure(EntityTypeBuilder<PartnerApplication> builder)
        {
            builder.HasOne(s => s.Approver)
                .WithMany()
                .HasForeignKey(s => s.ApproverId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.ToTable(nameof(PartnerApplication));

        }
    }
}