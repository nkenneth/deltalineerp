﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class vehicletorequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DepartureDate",
                table: "HireRequest",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AdditionalRequest",
                table: "HireRequest",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VehicleId",
                table: "HireRequest",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_HireRequest_VehicleId",
                table: "HireRequest",
                column: "VehicleId");

            migrationBuilder.AddForeignKey(
                name: "FK_HireRequest_Vehicle_VehicleId",
                table: "HireRequest",
                column: "VehicleId",
                principalTable: "Vehicle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HireRequest_Vehicle_VehicleId",
                table: "HireRequest");

            migrationBuilder.DropIndex(
                name: "IX_HireRequest_VehicleId",
                table: "HireRequest");

            migrationBuilder.DropColumn(
                name: "AdditionalRequest",
                table: "HireRequest");

            migrationBuilder.DropColumn(
                name: "VehicleId",
                table: "HireRequest");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DepartureDate",
                table: "HireRequest",
                nullable: true,
                oldClrType: typeof(DateTime));
        }
    }
}
