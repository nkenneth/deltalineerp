﻿namespace LME.Core.Entities.Enums
{
    public enum PartnershipEnquiryStatus
    {
        Initial,
        Rejected,
        Contacted,
        Inspected
    }
}