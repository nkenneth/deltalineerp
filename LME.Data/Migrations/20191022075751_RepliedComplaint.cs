﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class RepliedComplaint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RepliedMessage",
                table: "Complaint",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Responded",
                table: "Complaint",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RepliedMessage",
                table: "Complaint");

            migrationBuilder.DropColumn(
                name: "Responded",
                table: "Complaint");
        }
    }
}
