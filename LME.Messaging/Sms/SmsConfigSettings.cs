﻿using LME.Core.Configuration;

namespace LME.Messaging.Sms
{
    public abstract class SMSConfigSettings : ISettings
    {
        public string Sender { get; set; }
    }
}