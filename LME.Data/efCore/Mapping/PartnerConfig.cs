﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class PartnerConfig : IEntityTypeConfiguration<Partner>
    {
        public void Configure(EntityTypeBuilder<Partner> builder)
        {
           builder.HasOne(s => s.Wallet)
              .WithMany()
              .HasForeignKey(s => s.WalletId)
              .OnDelete(DeleteBehavior.ClientSetNull);

            builder.ToTable(nameof(Partner));

        }
    }
}