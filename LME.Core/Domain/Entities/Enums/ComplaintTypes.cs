﻿namespace LME.Core.Domain.Entities.Enums
{
    public enum ComplaintTypes
    {
        MissedTrips,
        TicketPayments,
        DriverService,
        BusServices,
        GeneralIssue,
        SuggestionOrAccolades
    }
}
