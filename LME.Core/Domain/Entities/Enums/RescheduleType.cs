﻿namespace LME.Core.Entities.Enums
{
    public enum RescheduleType
    {
        NotRescheduled,
        DroppedFromManifest,
        RescheduledIntoBus
    }
}