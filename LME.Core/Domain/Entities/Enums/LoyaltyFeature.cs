﻿namespace LME.Core.Entities.Enums
{
    public enum LoyaltyFeature
    {
        TravelFrequency,
        FareAmount
    }
}