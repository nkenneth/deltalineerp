﻿using LME.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace LME.Data.efCore.Mapping
{
    class MtuPhotoConfig : IEntityTypeConfiguration<MtuPhoto>
    {
        public void Configure(EntityTypeBuilder<MtuPhoto> builder)
        {
            builder.ToTable(nameof(MtuPhoto));

        }
    }
}
