﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class OTPFOREMPLOYEE : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "OTPLastUsedDate",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Otp",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "OtpIsUsed",
                table: "Employee",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "OtpNoOfTimeUsed",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TicketRemovalOtp",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "TicketRemovalOtpIsUsed",
                table: "Employee",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OTPLastUsedDate",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "Otp",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "OtpIsUsed",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "OtpNoOfTimeUsed",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "TicketRemovalOtp",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "TicketRemovalOtpIsUsed",
                table: "Employee");
        }
    }
}
