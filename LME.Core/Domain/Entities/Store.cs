﻿using LME.Core.Entities.Common;
using LME.Core.Entities.Enums;

namespace LME.Core.Entities
{
    public class Store : FullAuditedEntity
    {
        public StoreType Type { get; set; }
        public string Name { get; set; }

        public string StoreKeeper { get; set; }
        public int TerminalId { get; set; }
        public virtual Terminal Terminal { get; set; }
    }
}