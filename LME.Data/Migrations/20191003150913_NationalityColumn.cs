﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class NationalityColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Nationality",
                table: "SeatManagement",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nationality",
                table: "Booking",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nationality",
                table: "SeatManagement");

            migrationBuilder.DropColumn(
                name: "Nationality",
                table: "Booking");
        }
    }
}
