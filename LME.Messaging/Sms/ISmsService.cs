﻿namespace LME.Messaging.Sms
{
    public interface ISMSService
    {
        void SendSMSNow(string message, string sender = "", params string[] recipient);
    }
}