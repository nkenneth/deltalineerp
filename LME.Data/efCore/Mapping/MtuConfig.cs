﻿using LME.Core.Domain.Entities;
using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class MtuConfig : IEntityTypeConfiguration<MtuReportModel>
    {
        public void Configure(EntityTypeBuilder<MtuReportModel> builder)
        {
            builder.ToTable(nameof(MtuReportModel));

        }
    }
}
