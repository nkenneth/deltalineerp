﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class Sp_SalaryReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"CREATE PROCEDURE Sp_DriverSalary @DriverCode nvarchar(50), @StartDate DateTime, @EndDate DateTime
                        AS
                        BEGIN
                            -- SET NOCOUNT ON added to prevent extra result sets from
                            -- interfering with SELECT statements.
                            SET NOCOUNT ON

                           select distinct vtr.DriverCode, Sum(jm.CaptainFee) over (partition by vtr.DriverCode) as DriverFee 
, Count(*) over (partition by vtr.DriverCode) as NoofTrips,  driv.Name, driv.BankName, driv.BankAccount, @StartDate as StartDate, @EndDate as EndDate
                            from 
	                        JourneyManagement jm join 
		                        VehicleTripRegistration vtr on 
		                        jm.VehicleTripRegistrationId = vtr.Id
		                        join Trip tr on
		                        tr.id = vtr.TripId
		                        join Driver driv 
		                        on driv.Code = vtr.DriverCode
		                        where 
		                        vtr.DepartureDate between @StartDate and @EndDate
		                        --and (jm.JourneyStatus = 3) 
                                and 
		                        (@DriverCode is null or vtr.DriverCode = @DriverCode)
                        END
                        GO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
