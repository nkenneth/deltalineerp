﻿using LME.Core.Entities.Common;
using System;

namespace LME.Core.Entities
{
    public class DriverAccount: FullAuditedEntity<Guid>
    {
        public string DriverCode { get; set; }
        public string Password { get; set; }
        public string ConfirmationCode { get; set; }

        public string DeviceToken { get; set; }
    }
}