﻿namespace LME.Core.Entities.Enums
{
    public enum Gender
    {
        Male,
        Female
    }
}