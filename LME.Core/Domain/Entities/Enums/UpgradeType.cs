﻿namespace LME.Core.Entities.Enums
{
    public enum UpgradeType
    {
        Downgrade,
        Upgrade
    }
}