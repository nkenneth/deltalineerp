﻿namespace LME.Core.Entities.Enums
{
    public enum FareType
    {
        Discount,
        Increase
    }

    public enum FareAdjustmentType
    {
        Value,
        Percentage
    }

    public enum FareParameterType
    {
        Route,
        Terminal
    }
}