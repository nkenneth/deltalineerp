﻿using LME.Core.Entities.Common;
using LME.Core.Entities.Enums;
using System;

namespace LME.Core.Entities
{
    public class Terminal : FullAuditedEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Image { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonNo { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public DateTime TerminalStartDate { get; set; }

        public TerminalType TerminalType { get; set; }
        public int StateId { get; set; }
        public virtual State State { get; set; }
        public string TerminalCode { get; set; }
        public bool IsNew { get; set; }
        public string TicketRemovalOtp { get; set; }
        public bool TicketRemovalOtpIsUsed { get; set; }
    }
}