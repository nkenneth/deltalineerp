﻿using LME.Core.Entities.Common;

namespace LME.Core.Entities
{
    public class CustomerCouponRegistration : FullAuditedEntity<long>
    {
        public string CouponCode { get; set; }
        public string Username { get; set; }
        public string PhoneNumber { get; set; }
    }
}