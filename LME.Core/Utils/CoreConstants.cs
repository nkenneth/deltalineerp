﻿namespace LME.Core.Utils
{
    public abstract class CoreConstants
    {
        public const string DefaultAccount = "administrator@lme.com";
        public const string WebBookingAccount = "web@deltaline.com";
        public const string IosBookingAccount = "ios@deltaline.com";
        public const string AndroidBookingAccount = "android@deltaline.com";
        public const string DateFormat = "dd MMMM, yyyy";
        public const string TimeFormat = "hh:mm tt";
        public const string SystemDateFormat = "dd/MM/yyyy";

        public class Roles
        {
            public const string Admin = "Administrator";
            public const string Ticketer = "Ticketer";
            public const string OperationManager = "Operations Manager";
            public const string BookingManager = "Booking Manager";
            public const string Auditor = "Auditor";
            public const string CustomerCare = "Customer Care";
            public const string Accountant = "Accountant";
            public const string TerminalManager = "Terminal Manager";
        }

        public class Url
        {
            public const string PasswordResetEmail = "messaging/emailtemplates/password-resetcode-email.html";
            public const string AccountActivationEmail = "messaging/emailtemplates/account-email.html";
            public const string BookingSuccessEmail = "messaging/emailtemplates/confirm-email.html";
            public const string BookingAndReturnSuccessEmail = "messaging/emailtemplates/confirm-return-email.html";
            public const string ActivationCodeEmail = "messaging/emailtemplates/activation-code-email.html";
            public const string BookingUnSuccessEmail = "messaging/emailtemplates/failed-email.html";
            public const string RescheduleSuccessEmail = "messaging/emailtemplates/reschedule-success.html";
            public const string AdminHireBookingEmail = "messaging/emailtemplates/hirebooking-admin.html";
            public const string CustomerHireBookingEmail = "messaging/emailtemplates/hirebooking.html";
        }
    }
}