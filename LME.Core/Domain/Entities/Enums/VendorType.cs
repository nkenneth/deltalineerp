﻿namespace LME.Core.Entities.Enums
{
    public enum VendorType
    {
        Inventory,
        Workshop
    }
}