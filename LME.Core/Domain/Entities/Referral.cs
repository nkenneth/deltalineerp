﻿using LME.Core.Entities.Common;
using LME.Core.Entities.Enums;

namespace LME.Core.Entities
{
    public class Referral : FullAuditedEntity<long>
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set;}
        public UserType UserType { get; set; }
        public string  ReferralCode { get; set; }
    }
}