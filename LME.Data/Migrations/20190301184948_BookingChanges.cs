﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class BookingChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Position_PositionId",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Wallet_WalletId",
                table: "Employee");

            migrationBuilder.DropIndex(
                name: "IX_Employee_PositionId",
                table: "Employee");

            migrationBuilder.DropIndex(
                name: "IX_Employee_WalletId",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "PositionId",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "WalletId",
                table: "Employee");

            migrationBuilder.RenameColumn(
                name: "isSubReturn",
                table: "SeatManagement",
                newName: "IsSubReturn");

            migrationBuilder.RenameColumn(
                name: "isSub",
                table: "SeatManagement",
                newName: "IsSub");

            migrationBuilder.RenameColumn(
                name: "hasCoupon",
                table: "SeatManagement",
                newName: "HasCoupon");

            migrationBuilder.AddColumn<string>(
                name: "Photo",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Photo",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "IsSubReturn",
                table: "SeatManagement",
                newName: "isSubReturn");

            migrationBuilder.RenameColumn(
                name: "IsSub",
                table: "SeatManagement",
                newName: "isSub");

            migrationBuilder.RenameColumn(
                name: "HasCoupon",
                table: "SeatManagement",
                newName: "hasCoupon");

            migrationBuilder.AddColumn<int>(
                name: "PositionId",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WalletId",
                table: "Employee",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Employee_PositionId",
                table: "Employee",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_WalletId",
                table: "Employee",
                column: "WalletId");

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Position_PositionId",
                table: "Employee",
                column: "PositionId",
                principalTable: "Position",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Wallet_WalletId",
                table: "Employee",
                column: "WalletId",
                principalTable: "Wallet",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
