﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class Franchize : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_FranchiseUsers",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "AccessFailedCount",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "ConcurrencyStamp",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "EmailConfirmed",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "LockoutEnabled",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "LockoutEnd",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "NormalizedEmail",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "NormalizedUserName",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "PasswordHash",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "PhoneNumberConfirmed",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "SecurityStamp",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "TwoFactorEnabled",
                table: "FranchiseUsers");

            migrationBuilder.DropColumn(
                name: "UserName",
                table: "FranchiseUsers");

            migrationBuilder.RenameTable(
                name: "FranchiseUsers",
                newName: "FranchiseUser");

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "FranchiseUser",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "FranchiseUser",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "FranchiseUser",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_FranchiseUser",
                table: "FranchiseUser",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Franchize",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    IsFirstTimeLogin = table.Column<bool>(nullable: false),
                    OptionalPhoneNumber = table.Column<string>(nullable: true),
                    UserType = table.Column<int>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    RefreshToken = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    DeviceToken = table.Column<string>(nullable: true),
                    Referrer = table.Column<string>(nullable: true),
                    ReferralCode = table.Column<string>(nullable: true),
                    NextOfKinName = table.Column<string>(nullable: true),
                    NextOfKinPhone = table.Column<string>(nullable: true),
                    LoginDeviceType = table.Column<int>(nullable: false),
                    WalletId = table.Column<int>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    DateOfBirth = table.Column<string>(nullable: true),
                    AccountConfirmationCode = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    OTP = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchize", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Franchize");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FranchiseUser",
                table: "FranchiseUser");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "FranchiseUser");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "FranchiseUser");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "FranchiseUser");

            migrationBuilder.RenameTable(
                name: "FranchiseUser",
                newName: "FranchiseUsers");

            migrationBuilder.AddColumn<int>(
                name: "AccessFailedCount",
                table: "FranchiseUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ConcurrencyStamp",
                table: "FranchiseUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "FranchiseUsers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "EmailConfirmed",
                table: "FranchiseUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "LockoutEnabled",
                table: "FranchiseUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LockoutEnd",
                table: "FranchiseUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NormalizedEmail",
                table: "FranchiseUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NormalizedUserName",
                table: "FranchiseUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PasswordHash",
                table: "FranchiseUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "FranchiseUsers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "PhoneNumberConfirmed",
                table: "FranchiseUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "SecurityStamp",
                table: "FranchiseUsers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "TwoFactorEnabled",
                table: "FranchiseUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "FranchiseUsers",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_FranchiseUsers",
                table: "FranchiseUsers",
                column: "Id");
        }
    }
}
