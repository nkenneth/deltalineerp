﻿using LME.Business.Services;
using LME.Core.DataTransferObjects;
using LME.Core.Domain.DataTransferObjects;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LME.WebAPI.Controllers
{
    public class CustomerController : BaseController
    {
        readonly ICustomerService _customerSvc;
        readonly IUserService _userSvc;

        public CustomerController(ICustomerService customerSvc, IUserService userSvc)
        {
            _customerSvc = customerSvc;
            _userSvc = userSvc;
        }

        [HttpPost]
        [Route("Add")]
        public async Task<ServiceResponse<UserDTO>> CreateUserProfile(CustomerDTO profile)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var result = await _customerSvc.CreateAccount(profile);
                return new ServiceResponse<UserDTO>(result);
            });
        }

        [HttpPost]
        [Route("SendActivationCode")]
        public async Task<ServiceResponse<bool>> SendActivationCode(string userNameOrEmail)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _customerSvc.SendActivationCode(userNameOrEmail);
                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetCustomer/{phoneNumber}")]
        public async Task<ServiceResponse<CustomerDTO>> GetCustomer(string phoneNumber)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var result = await _customerSvc.GetCustomer(phoneNumber);
                return new ServiceResponse<CustomerDTO>(result);
            });
        }
    }
}