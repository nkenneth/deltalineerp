﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class Sp_HireRequests : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"CREATE PROCEDURE Sp_HiredRequests
		@Keyword nvarchar(200), @StartDate DateTime , @EndDate DateTime	, @PageIndex int, @PageSize int
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

    -- Insert statements for procedure here
    select CreationTime, FirstName, LastName, MiddleName, Email, PhoneNumber, Address, hr.NextOfKinName, NextofkinPhoneNumber
--should join on vehicle later
Departure, Destination, AdditionalRequest,  [TotalCount]= COUNT(*) OVER()
 from HireRequest hr

 where (@Keyword is null) or 
    (
	
	 (hr.FirstName like '%' + @Keyword + '%') or
	 (hr.LastName like '%' + @Keyword + '%') or
	 (hr.Email like '%' + @Keyword + '%') or
	 (hr.PhoneNumber like '%' + @Keyword + '%') or
	 (hr.Destination like '%' + @Keyword + '%')
	 )
	 and CreationTime Between @StartDate and @EndDate 
     ORDER BY Hr.CreationTime desc
	 OFFSET (@PageIndex - 1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY
END
GO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
