﻿using Microsoft.AspNetCore.Mvc;

namespace LME.WebAPI.Controllers
{
    public class HomeController : BaseController
    {

        [HttpGet]
        public IActionResult Index()
        {
            return Ok("LME.Web Api is running");
        }
    }
}