﻿using LME.Core.Extensions;
using LME.Core.Timing;
using LME.Messaging.Model;
using LME.Messaging.Sms.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LME.Messaging.Sms
{
    public class SMSService : ISMSService
    {
        public static IWebClient WebClientSource;

        public SMSService(IWebClient webClient)
        {
            WebClientSource = webClient;
        }

        public void SendSMSNow(string message, string sender = "", params string[] recipient)
        {
            var model = new SMSLiveModel
            {
                Message = message,
                Sender = sender,
                Recipient = recipient
            };

            //Task.WaitAll(KonnectKirusaSMS(model).SendSmsAsync());
            Task.WaitAll(OgoSMS(model).SendSmsAsync());
        }

        public SMSSenderModel OgoSMS(SMSLiveModel model)
        {
            var smsBody = model.Message;
            var recipient = model.Recipient.ArrayToCommaSeparatedString().UrlEncode();
            

            var url = "http://www.ogosms.com/dynamicapi/?username=deltaline&password=Delta1212&sender=DELTALINE&numbers=" + recipient + "&message=" + smsBody;
            var senderModel = new SMSSenderModel
            {
                ApiUrl = url,
                Method = "GET"
            };
            return senderModel;
        }

    }
}