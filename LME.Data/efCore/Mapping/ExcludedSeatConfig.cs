﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class ExcludedSeatConfig : IEntityTypeConfiguration<ExcludedSeat>
    {
        public void Configure(EntityTypeBuilder<ExcludedSeat> builder)
        {
            builder.ToTable(nameof(ExcludedSeat));
        }
    }
}