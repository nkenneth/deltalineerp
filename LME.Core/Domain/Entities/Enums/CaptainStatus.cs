﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LME.Core.Entities.Enums
{
   public enum DriverStatus
    {
        Idle,
        OnAJourney,
        Suspended,
        OnLeave,
        Dismissal,
        Decease
    }
}
