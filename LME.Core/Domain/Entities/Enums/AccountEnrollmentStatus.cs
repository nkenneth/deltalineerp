﻿namespace LME.Core.Entities.Enums
{
    public enum AccountEnrollmentStatus
    {
        RequestEnrollment,
        EnrollmentConfirmation,
        EnrollmentFailed,
        Enrolled
    }
}