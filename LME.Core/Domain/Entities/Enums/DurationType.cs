﻿namespace LME.Core.Entities.Enums
{
    public enum DurationType
    {
        Second,
        Minute,
        Hour,
        Day,
        Month,
        Year,
        Week
    }
}