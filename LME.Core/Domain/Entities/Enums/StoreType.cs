﻿namespace LME.Core.Entities.Enums
{
    public enum StoreType
    {
        RegularStore,
        Hub,
        CentralInventory,
        Patrol
    }
}