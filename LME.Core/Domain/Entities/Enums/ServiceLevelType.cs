﻿namespace LME.Core.Entities.Enums
{
    public enum ServiceLevelType
    {
        Level_A,
        Level_B,
        Level_C,
        Level_D,
        NONE = 100
    }
}