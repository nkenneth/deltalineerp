﻿using LME.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace LME.Core.Domain.Entities
{
    public class MtuPhoto: FullAuditedEntity
    {
        public string FileName { get; set; }
        public int MtuReportModelId { get; set; }
    }
}
