﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace LME.Data.efCore.Mapping
{
    class DiscountConfig : IEntityTypeConfiguration<Discount>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Discount> builder)
        {
            builder.HasKey(x => x.Id);
            builder.ToTable(nameof(Discount));

        }
    }
}
