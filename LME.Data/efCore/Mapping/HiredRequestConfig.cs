﻿using LME.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LME.Data.efCore.Mapping
{
    class HiredRequestConfig : IEntityTypeConfiguration<HireRequest>
    {
        public void Configure(EntityTypeBuilder<HireRequest> builder)
        {
            builder.ToTable(nameof(HireRequest));

        }
    }
}