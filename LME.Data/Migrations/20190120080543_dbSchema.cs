﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class dbSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccountSummary",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AccountName = table.Column<string>(nullable: true),
                    Balance = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountSummary", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AccountTransaction",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    Narration = table.Column<string>(nullable: true),
                    AccountType = table.Column<int>(nullable: false),
                    TransactionType = table.Column<int>(nullable: false),
                    TransactionSourceId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountTransaction", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    IsFirstTimeLogin = table.Column<bool>(nullable: false),
                    OptionalPhoneNumber = table.Column<string>(nullable: true),
                    UserType = table.Column<int>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Bank",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccountType = table.Column<string>(nullable: true),
                    AccountNumber = table.Column<string>(nullable: true),
                    AccountName = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bank", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BookingType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Coupon",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    CouponCode = table.Column<string>(nullable: true),
                    CouponValue = table.Column<decimal>(nullable: false),
                    CouponType = table.Column<int>(nullable: false),
                    Validity = table.Column<bool>(nullable: false),
                    DurationType = table.Column<int>(nullable: false),
                    Duration = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coupon", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomerCouponRegistration",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CouponCode = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerCouponRegistration", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Department",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Department", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Discount",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    BookingType = table.Column<int>(nullable: false),
                    AdultDiscount = table.Column<decimal>(nullable: false),
                    MinorDiscount = table.Column<decimal>(nullable: false),
                    MemberDiscount = table.Column<decimal>(nullable: false),
                    ReturnDiscount = table.Column<decimal>(nullable: false),
                    AppDiscountIos = table.Column<decimal>(nullable: false),
                    AppDiscountAndroid = table.Column<decimal>(nullable: false),
                    AppDiscountWeb = table.Column<decimal>(nullable: false),
                    AppReturnDiscountIos = table.Column<decimal>(nullable: false),
                    AppReturnDiscountAndroid = table.Column<decimal>(nullable: false),
                    AppReturnDiscountWeb = table.Column<decimal>(nullable: false),
                    PromoDiscount = table.Column<decimal>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Discount", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DriverAccount",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DriverCode = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    ConfirmationCode = table.Column<string>(nullable: true),
                    DeviceToken = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverAccount", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GeneralLedger",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    TransactionTypeId = table.Column<int>(nullable: false),
                    TransactionSourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralLedger", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IdentificationType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentificationType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PartnerEnquiry",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NumberOfVehicles = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    BusinessAddress = table.Column<string>(nullable: true),
                    VehicleSpec = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    CompanyRcNumber = table.Column<string>(nullable: true),
                    PartnershipEnquiryStatus = table.Column<int>(nullable: false),
                    InspectionLocation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartnerEnquiry", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentGatewayStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Gateway = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentGatewayStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PayStackPaymentResponse",
                columns: table => new
                {
                    Reference = table.Column<string>(nullable: false),
                    ApprovedAmount = table.Column<int>(nullable: false),
                    AuthorizationCode = table.Column<string>(nullable: true),
                    CardType = table.Column<string>(nullable: true),
                    Last4 = table.Column<string>(nullable: true),
                    Reusable = table.Column<bool>(nullable: false),
                    Bank = table.Column<string>(nullable: true),
                    ExpireMonth = table.Column<string>(nullable: true),
                    ExpireYear = table.Column<string>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    Channel = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PayStackPaymentResponse", x => x.Reference);
                });

            migrationBuilder.CreateTable(
                name: "PayStackWebhookResponse",
                columns: table => new
                {
                    Reference = table.Column<string>(nullable: false),
                    ApprovedAmount = table.Column<int>(nullable: false),
                    AuthorizationCode = table.Column<string>(nullable: true),
                    CardType = table.Column<string>(nullable: true),
                    Last4 = table.Column<string>(nullable: true),
                    Reusable = table.Column<bool>(nullable: false),
                    Bank = table.Column<string>(nullable: true),
                    ExpireMonth = table.Column<string>(nullable: true),
                    ExpireYear = table.Column<string>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    Channel = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PayStackWebhookResponse", x => x.Reference);
                });

            migrationBuilder.CreateTable(
                name: "Position",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Position", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Region",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Region", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TripSetting",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TripSettingId = table.Column<Guid>(nullable: false),
                    RouteId = table.Column<int>(nullable: false),
                    WeekDays = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripSetting", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleMake",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleMake", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleMileage",
                columns: table => new
                {
                    VehicleRegistrationNumber = table.Column<string>(nullable: false),
                    ServiceLevel = table.Column<int>(nullable: false),
                    Id = table.Column<Guid>(nullable: false),
                    CurrentMileage = table.Column<int>(nullable: false),
                    LastServiceDate = table.Column<DateTime>(nullable: true),
                    DateDue = table.Column<DateTime>(nullable: true),
                    IsDue = table.Column<bool>(nullable: false),
                    IsDeactivated = table.Column<bool>(nullable: false),
                    NotificationLevel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleMileage", x => new { x.VehicleRegistrationNumber, x.ServiceLevel });
                });

            migrationBuilder.CreateTable(
                name: "Vendor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ContactName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    CompanyRegistrationNumber = table.Column<string>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    BankAccountNumber = table.Column<string>(nullable: true),
                    VendorType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vendor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Wallet",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    WalletNumber = table.Column<string>(nullable: true),
                    Balance = table.Column<decimal>(nullable: false),
                    UserType = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    IsReset = table.Column<bool>(nullable: false),
                    LastResetDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wallet", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WalletNumber",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    WalletPan = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WalletNumber", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Booking",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    PosReference = table.Column<string>(nullable: true),
                    BookingReferenceCode = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    NoOfTicket = table.Column<int>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PaymentMethod = table.Column<int>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    NextOfKinName = table.Column<string>(nullable: true),
                    NextOfKinPhoneNumber = table.Column<string>(nullable: true),
                    NumberOfTicketsPrinted = table.Column<int>(nullable: false),
                    PickupPointImage = table.Column<string>(nullable: true),
                    BookingDate = table.Column<DateTime>(nullable: false),
                    PaymentGateway = table.Column<int>(nullable: false),
                    SelectedSeats = table.Column<string>(nullable: true),
                    PayStackReference = table.Column<string>(nullable: true),
                    PayStackResponse = table.Column<string>(nullable: true),
                    PayStackWebhookReference = table.Column<string>(nullable: true),
                    GtbReference = table.Column<string>(nullable: true),
                    FlutterWaveReference = table.Column<string>(nullable: true),
                    FlutterWaveResponse = table.Column<string>(nullable: true),
                    ApprovedBy = table.Column<string>(nullable: true),
                    GlobalPayReference = table.Column<string>(nullable: true),
                    GlobalPayResponse = table.Column<string>(nullable: true),
                    QuickTellerReference = table.Column<string>(nullable: true),
                    QuickTellerResponse = table.Column<string>(nullable: true),
                    BookingStatus = table.Column<int>(nullable: false),
                    PassengerType = table.Column<int>(nullable: false),
                    PickupStatus = table.Column<int>(nullable: false),
                    BookingTypeId = table.Column<int>(nullable: true),
                    TravelStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Booking", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Booking_BookingType_BookingTypeId",
                        column: x => x.BookingTypeId,
                        principalTable: "BookingType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Booking_PayStackPaymentResponse_PayStackReference",
                        column: x => x.PayStackReference,
                        principalTable: "PayStackPaymentResponse",
                        principalColumn: "Reference",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Booking_PayStackWebhookResponse_PayStackWebhookReference",
                        column: x => x.PayStackWebhookReference,
                        principalTable: "PayStackWebhookResponse",
                        principalColumn: "Reference",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "State",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    RegionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_State", x => x.Id);
                    table.ForeignKey(
                        name: "FK_State_Region_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Region",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleModel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    VehicleModelName = table.Column<string>(nullable: true),
                    NumberOfSeats = table.Column<int>(nullable: false),
                    VehicleModelTypeCode = table.Column<string>(nullable: true),
                    VehicleMakeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleModel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleModel_VehicleMake_VehicleMakeId",
                        column: x => x.VehicleMakeId,
                        principalTable: "VehicleMake",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    CustomerCode = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    OptionalPhoneNumber = table.Column<string>(nullable: true),
                    NextOfKinName = table.Column<string>(nullable: true),
                    NextOfKinPhone = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    AccountConfirmationCode = table.Column<string>(nullable: true),
                    AccountIsConfirmed = table.Column<bool>(nullable: true),
                    LoginDeviceType = table.Column<int>(nullable: false),
                    WalletId = table.Column<int>(nullable: true),
                    DeviceToken = table.Column<string>(nullable: true),
                    ReferralCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customer_Wallet_WalletId",
                        column: x => x.WalletId,
                        principalTable: "Wallet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Driver",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    HandoverCode = table.Column<string>(nullable: true),
                    VehicleRegistrationNumber = table.Column<string>(nullable: true),
                    DriverStatus = table.Column<int>(nullable: false),
                    DriverType = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Phone1 = table.Column<string>(nullable: true),
                    Phone2 = table.Column<string>(nullable: true),
                    Designation = table.Column<string>(nullable: true),
                    AssignedDate = table.Column<DateTime>(nullable: false),
                    ResidentialAddress = table.Column<string>(nullable: true),
                    NextOfKin = table.Column<string>(nullable: true),
                    DateOfEmployment = table.Column<DateTime>(nullable: true),
                    Picture = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    NextOfKinNumber = table.Column<string>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    BankAccount = table.Column<string>(nullable: true),
                    DeactivationReason = table.Column<string>(nullable: true),
                    ActivationStatusChangedByEmail = table.Column<string>(nullable: true),
                    WalletId = table.Column<int>(nullable: false),
                    MaintenanceWalletId = table.Column<int>(nullable: true),
                    NoOfTrips = table.Column<int>(nullable: false),
                    EnrollmentStatus = table.Column<int>(nullable: false),
                    ConfirmationCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Driver", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Driver_Wallet_MaintenanceWalletId",
                        column: x => x.MaintenanceWalletId,
                        principalTable: "Wallet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Driver_Wallet_WalletId",
                        column: x => x.WalletId,
                        principalTable: "Wallet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WalletTransaction",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TransactionType = table.Column<int>(nullable: false),
                    TransactionSourceId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    TransactionAmount = table.Column<decimal>(nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    LineBalance = table.Column<decimal>(nullable: false),
                    WalletId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WalletTransaction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WalletTransaction_Wallet_WalletId",
                        column: x => x.WalletId,
                        principalTable: "Wallet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Terminal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    ContactPersonNo = table.Column<string>(nullable: true),
                    Latitude = table.Column<float>(nullable: false),
                    Longitude = table.Column<float>(nullable: false),
                    TerminalStartDate = table.Column<DateTime>(nullable: false),
                    TerminalType = table.Column<int>(nullable: false),
                    StateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Terminal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Terminal_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehiclePart",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    CheckThreshold = table.Column<int>(nullable: false),
                    RefillThreshold = table.Column<int>(nullable: false),
                    HubCheckThreshold = table.Column<int>(nullable: false),
                    HubRefillThreshold = table.Column<int>(nullable: false),
                    CentralCheckThreshold = table.Column<int>(nullable: false),
                    CentralRefillThreshold = table.Column<int>(nullable: false),
                    VehicleModelId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehiclePart", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehiclePart_VehicleModel_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BankPayment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    PaymentDate = table.Column<DateTime>(nullable: false),
                    BankId = table.Column<int>(nullable: false),
                    TellerNumber = table.Column<string>(nullable: true),
                    Depositor = table.Column<string>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    TerminalId = table.Column<int>(nullable: false),
                    AccountingStatus = table.Column<int>(nullable: false),
                    AuthorisedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankPayment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BankPayment_Bank_BankId",
                        column: x => x.BankId,
                        principalTable: "Bank",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankPayment_Terminal_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "Terminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CashRemittant",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    Ticketer = table.Column<string>(nullable: true),
                    Accountant = table.Column<string>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    TerminalId = table.Column<int>(nullable: false),
                    AccountingStatus = table.Column<int>(nullable: false),
                    AuthorisedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashRemittant", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CashRemittant_Terminal_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "Terminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Expense",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Receiver = table.Column<string>(nullable: true),
                    Issuer = table.Column<string>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    TerminalId = table.Column<int>(nullable: false),
                    AccountingStatus = table.Column<int>(nullable: false),
                    AuthorisedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Expense", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Expense_Terminal_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "Terminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OtherIncome",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    PaymentName = table.Column<string>(nullable: true),
                    PaymentDescription = table.Column<string>(nullable: true),
                    Issuer = table.Column<string>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    TerminalId = table.Column<int>(nullable: false),
                    TerminalName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OtherIncome", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OtherIncome_Terminal_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "Terminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Route",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    DispatchFee = table.Column<decimal>(nullable: false),
                    CaptainFee = table.Column<decimal>(nullable: false),
                    LoaderFee = table.Column<decimal>(nullable: false),
                    AvailableAtTerminal = table.Column<bool>(nullable: false),
                    AvailableOnline = table.Column<bool>(nullable: false),
                    ParentRouteId = table.Column<int>(nullable: true),
                    ParentRoute = table.Column<string>(nullable: true),
                    DepartureTerminalId = table.Column<int>(nullable: false),
                    DestinationTerminalId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Route", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Route_Terminal_DepartureTerminalId",
                        column: x => x.DepartureTerminalId,
                        principalTable: "Terminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Route_Terminal_DestinationTerminalId",
                        column: x => x.DestinationTerminalId,
                        principalTable: "Terminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Store",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    StoreKeeper = table.Column<string>(nullable: true),
                    TerminalId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Store_Terminal_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "Terminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehiclePartPosition",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Position = table.Column<string>(nullable: true),
                    VehiclePartId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehiclePartPosition", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehiclePartPosition_VehiclePart_VehiclePartId",
                        column: x => x.VehiclePartId,
                        principalTable: "VehiclePart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Fare",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    ChildrenDiscountPercentage = table.Column<int>(nullable: true),
                    RouteId = table.Column<int>(nullable: false),
                    VehicleModelId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fare", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Fare_Route_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Route",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Fare_VehicleModel_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FareCalendar",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    FareType = table.Column<int>(nullable: false),
                    FareValue = table.Column<decimal>(nullable: false),
                    RouteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FareCalendar", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FareCalendar_Route_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Route",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubRoute",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    SubRouteNameId = table.Column<int>(nullable: false),
                    SubRouteName = table.Column<string>(nullable: true),
                    RouteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubRoute", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubRoute_Route_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Route",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Trip",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DepartureTime = table.Column<string>(nullable: true),
                    TripCode = table.Column<string>(nullable: true),
                    AvailableOnline = table.Column<bool>(nullable: false),
                    ParentRouteDepartureTime = table.Column<string>(nullable: true),
                    RouteId = table.Column<int>(nullable: false),
                    VehicleModelId = table.Column<int>(nullable: true),
                    ParentRouteId = table.Column<int>(nullable: true),
                    ParentTripId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trip", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Trip_Route_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Route",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Trip_VehicleModel_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PickupPoint",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PickupTime = table.Column<string>(nullable: true),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    TripId = table.Column<Guid>(nullable: false),
                    TerminalId = table.Column<int>(nullable: true),
                    RouteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PickupPoint", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PickupPoint_Route_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Route",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PickupPoint_Terminal_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "Terminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PickupPoint_Trip_TripId",
                        column: x => x.TripId,
                        principalTable: "Trip",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TripAvailability",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    EbmUsername = table.Column<string>(nullable: true),
                    AssginedVehicle = table.Column<string>(nullable: true),
                    TripSettingId = table.Column<Guid>(nullable: false),
                    TripId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripAvailability", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TripAvailability_Trip_TripId",
                        column: x => x.TripId,
                        principalTable: "Trip",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TripAvailability_TripSetting_TripSettingId",
                        column: x => x.TripSettingId,
                        principalTable: "TripSetting",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTripRegistration",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PhysicalBusRegistrationNumber = table.Column<string>(nullable: true),
                    DepartureDate = table.Column<DateTime>(nullable: false),
                    IsVirtualBus = table.Column<bool>(nullable: false),
                    IsBusFull = table.Column<bool>(nullable: false),
                    IsBlownBus = table.Column<bool>(nullable: false),
                    ManifestPrinted = table.Column<bool>(nullable: false),
                    CaptainCode = table.Column<string>(nullable: true),
                    OriginalCaptainCode = table.Column<string>(nullable: true),
                    BookingTypeId = table.Column<int>(nullable: true),
                    JourneyType = table.Column<int>(nullable: false),
                    TripId = table.Column<Guid>(nullable: false),
                    VehicleModelId = table.Column<int>(nullable: true),
                    BookingId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTripRegistration", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleTripRegistration_Booking_BookingId",
                        column: x => x.BookingId,
                        principalTable: "Booking",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleTripRegistration_BookingType_BookingTypeId",
                        column: x => x.BookingTypeId,
                        principalTable: "BookingType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleTripRegistration_Trip_TripId",
                        column: x => x.TripId,
                        principalTable: "Trip",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehicleTripRegistration_VehicleModel_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HiredBooking",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    BookingCode = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    NextOfKinName = table.Column<string>(nullable: true),
                    NextOfKinPhoneNumber = table.Column<string>(nullable: true),
                    NumberOfPrintedTickets = table.Column<int>(nullable: false),
                    PickUpPointImage = table.Column<string>(nullable: true),
                    BookingDate = table.Column<DateTime>(nullable: false),
                    TravelStatus = table.Column<int>(nullable: false),
                    BookingStatus = table.Column<int>(nullable: false),
                    PickupStatus = table.Column<int>(nullable: false),
                    PickupPointId = table.Column<int>(nullable: false),
                    VehicleTripRegistrationId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HiredBooking", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HiredBooking_PickupPoint_PickupPointId",
                        column: x => x.PickupPointId,
                        principalTable: "PickupPoint",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HiredBooking_VehicleTripRegistration_VehicleTripRegistrationId",
                        column: x => x.VehicleTripRegistrationId,
                        principalTable: "VehicleTripRegistration",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JourneyManagement",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ActualTripStartTime = table.Column<DateTime>(nullable: true),
                    TripStartTime = table.Column<DateTime>(nullable: true),
                    TripEndTime = table.Column<DateTime>(nullable: true),
                    TransloadedJourneyId = table.Column<Guid>(nullable: true),
                    JourneyDate = table.Column<DateTime>(nullable: false),
                    ApprovedBy = table.Column<string>(nullable: true),
                    ReceivedBy = table.Column<string>(nullable: true),
                    DispatchFee = table.Column<decimal>(nullable: false),
                    CaptainFee = table.Column<decimal>(nullable: false),
                    LoaderFee = table.Column<decimal>(nullable: false),
                    VehicleTripRegistrationId = table.Column<Guid>(nullable: false),
                    JourneyStatus = table.Column<int>(nullable: false),
                    DenialReason = table.Column<string>(nullable: true),
                    JourneyType = table.Column<int>(nullable: false),
                    CaptainTripStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JourneyManagement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JourneyManagement_VehicleTripRegistration_VehicleTripRegistrationId",
                        column: x => x.VehicleTripRegistrationId,
                        principalTable: "VehicleTripRegistration",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ManifestManagement",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    NumberOfSeats = table.Column<int>(nullable: false),
                    IsPrinted = table.Column<bool>(nullable: false),
                    Amount = table.Column<decimal>(nullable: true),
                    Dispatch = table.Column<decimal>(nullable: true),
                    ManifestPrintedTime = table.Column<DateTime>(nullable: true),
                    VehicleTripRegistrationId = table.Column<Guid>(nullable: false),
                    VehicleModelId = table.Column<int>(nullable: true),
                    Employee = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManifestManagement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ManifestManagement_VehicleModel_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ManifestManagement_VehicleTripRegistration_VehicleTripRegistrationId",
                        column: x => x.VehicleTripRegistrationId,
                        principalTable: "VehicleTripRegistration",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SeatManagement",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SeatNumber = table.Column<byte>(nullable: false),
                    RemainingSeat = table.Column<int>(nullable: false),
                    BookingReferenceCode = table.Column<string>(nullable: true),
                    MainBookerReferenceCode = table.Column<string>(nullable: true),
                    RescheduleReferenceCode = table.Column<string>(nullable: true),
                    RerouteReferenceCode = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    NextOfKinName = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    PartCash = table.Column<decimal>(nullable: true),
                    RerouteFeeDiff = table.Column<decimal>(nullable: true),
                    UpgradeDowngradeDiff = table.Column<decimal>(nullable: true),
                    Discount = table.Column<decimal>(nullable: false),
                    NextOfKinPhoneNumber = table.Column<string>(nullable: true),
                    PickupPointImage = table.Column<string>(nullable: true),
                    ReschedulePayStackResponse = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    LuggageType = table.Column<string>(nullable: true),
                    Rated = table.Column<bool>(nullable: false),
                    FullName = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    PassengerType = table.Column<int>(nullable: false),
                    PaymentMethod = table.Column<int>(nullable: false),
                    BookingStatus = table.Column<int>(nullable: false),
                    PickupStatus = table.Column<int>(nullable: false),
                    BookingType = table.Column<int>(nullable: false),
                    TravelStatus = table.Column<int>(nullable: false),
                    UpgradeType = table.Column<int>(nullable: false),
                    RescheduleStatus = table.Column<int>(nullable: false),
                    RescheduleMode = table.Column<int>(nullable: false),
                    RerouteStatus = table.Column<int>(nullable: false),
                    RerouteMode = table.Column<int>(nullable: false),
                    RescheduleType = table.Column<int>(nullable: false),
                    RescheduleDate = table.Column<DateTime>(nullable: true),
                    IsPrinted = table.Column<bool>(nullable: false),
                    IsRescheduled = table.Column<bool>(nullable: false),
                    IsRerouted = table.Column<bool>(nullable: false),
                    IsUpgradeDowngrade = table.Column<bool>(nullable: false),
                    IsMainBooker = table.Column<bool>(nullable: false),
                    HasReturn = table.Column<bool>(nullable: false),
                    IsReturn = table.Column<bool>(nullable: false),
                    FromTransload = table.Column<bool>(nullable: false),
                    RouteId = table.Column<int>(nullable: true),
                    isSub = table.Column<bool>(nullable: false),
                    isSubReturn = table.Column<bool>(nullable: false),
                    OnlineSubRouteName = table.Column<string>(nullable: true),
                    PickUpPointId = table.Column<int>(nullable: true),
                    NoOfTicket = table.Column<int>(nullable: true),
                    SubRouteId = table.Column<int>(nullable: true),
                    VehicleTripRegistrationId = table.Column<Guid>(nullable: true),
                    VehicleModelId = table.Column<int>(nullable: true),
                    BookingId = table.Column<long>(nullable: true),
                    BookingId1 = table.Column<int>(nullable: true),
                    hasCoupon = table.Column<bool>(nullable: false),
                    CouponCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeatManagement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SeatManagement_Booking_BookingId1",
                        column: x => x.BookingId1,
                        principalTable: "Booking",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SeatManagement_PickupPoint_PickUpPointId",
                        column: x => x.PickUpPointId,
                        principalTable: "PickupPoint",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SeatManagement_Route_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Route",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SeatManagement_SubRoute_SubRouteId",
                        column: x => x.SubRouteId,
                        principalTable: "SubRoute",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SeatManagement_VehicleModel_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SeatManagement_VehicleTripRegistration_VehicleTripRegistrationId",
                        column: x => x.VehicleTripRegistrationId,
                        principalTable: "VehicleTripRegistration",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    EmployeeCode = table.Column<string>(nullable: true),
                    DateJoined = table.Column<DateTime>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Otp = table.Column<string>(nullable: true),
                    OtpIsUsed = table.Column<bool>(nullable: false),
                    TicketRemovalOtp = table.Column<string>(nullable: true),
                    TicketRemovalOtpIsUsed = table.Column<bool>(nullable: false),
                    EmployeePhoto = table.Column<string>(nullable: true),
                    NextOfKin = table.Column<string>(nullable: true),
                    NextOfKinPhone = table.Column<string>(nullable: true),
                    OTPLastUsedDate = table.Column<DateTime>(nullable: true),
                    OtpNoOfTimeUsed = table.Column<int>(nullable: true),
                    WalletId = table.Column<int>(nullable: true),
                    DepartmentId = table.Column<int>(nullable: true),
                    PartnerId = table.Column<int>(nullable: true),
                    PositionId = table.Column<int>(nullable: true),
                    TerminalId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employee_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee_Position_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Position",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee_Terminal_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "Terminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee_Wallet_WalletId",
                        column: x => x.WalletId,
                        principalTable: "Wallet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeRoute",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EmployeeRouteId = table.Column<long>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    TerminalId = table.Column<int>(nullable: true),
                    EmployeeId = table.Column<int>(nullable: true),
                    RouteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeRoute", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmployeeRoute_Employee_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeeRoute_Route_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Route",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeeRoute_Terminal_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "Terminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PartnerApplication",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    CompanyRcNumber = table.Column<string>(nullable: true),
                    IdentificationNumber = table.Column<string>(nullable: true),
                    PartnerType = table.Column<int>(nullable: false),
                    TellAboutYou = table.Column<string>(nullable: true),
                    IsRegistered = table.Column<bool>(nullable: false),
                    PartnerApplicationStatus = table.Column<int>(nullable: false),
                    IdentificationTypeId = table.Column<int>(nullable: true),
                    ApproverId = table.Column<int>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    AccountNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartnerApplication", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartnerApplication_Employee_ApproverId",
                        column: x => x.ApproverId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PartnerApplication_IdentificationType_IdentificationTypeId",
                        column: x => x.IdentificationTypeId,
                        principalTable: "IdentificationType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Partner",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    OptionalPhoneNumber = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    PartnerType = table.Column<int>(nullable: false),
                    PartnerApplicationId = table.Column<int>(nullable: false),
                    WalletId = table.Column<int>(nullable: false),
                    AccountNumber = table.Column<string>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    Bank = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Partner", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Partner_PartnerApplication_PartnerApplicationId",
                        column: x => x.PartnerApplicationId,
                        principalTable: "PartnerApplication",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Partner_Wallet_WalletId",
                        column: x => x.WalletId,
                        principalTable: "Wallet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PartnersWallet",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    PartnerCode = table.Column<Guid>(nullable: false),
                    WalletNumber = table.Column<string>(nullable: true),
                    OperationalBalance = table.Column<decimal>(nullable: false),
                    Balance = table.Column<decimal>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    PartnerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartnersWallet", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartnersWallet_Partner_PartnerId",
                        column: x => x.PartnerId,
                        principalTable: "Partner",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vehicle",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    VehicleRegistrationNumber = table.Column<string>(nullable: true),
                    VehicleChasisNumber = table.Column<string>(nullable: true),
                    VehicleEngineNumber = table.Column<string>(nullable: true),
                    VehicleIMEINumber = table.Column<string>(nullable: true),
                    VehicleType = table.Column<string>(nullable: true),
                    VehicleStatus = table.Column<int>(nullable: false),
                    EBM = table.Column<string>(nullable: true),
                    PartnerId = table.Column<int>(nullable: false),
                    VehicleModelId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: true),
                    IsOperational = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicle", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vehicle_Terminal_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Terminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicle_Partner_PartnerId",
                        column: x => x.PartnerId,
                        principalTable: "Partner",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Vehicle_VehicleModel_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PartnersWalletTransaction",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    WalletId = table.Column<int>(nullable: false),
                    PartnerId = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    TransactionType = table.Column<int>(nullable: false),
                    CreatededBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    SourceId = table.Column<string>(nullable: true),
                    PartnersWalletId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartnersWalletTransaction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartnersWalletTransaction_PartnersWallet_PartnersWalletId",
                        column: x => x.PartnersWalletId,
                        principalTable: "PartnersWallet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PartnersWalletTransaction_PartnersWallet_WalletId",
                        column: x => x.WalletId,
                        principalTable: "PartnersWallet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehiclePartRegistration",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    VehiclePartName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    MaintenanceTimeFrameType = table.Column<int>(nullable: false),
                    InstallationMileage = table.Column<string>(nullable: true),
                    PartExpiryMileage = table.Column<string>(nullable: true),
                    PartInstallationDate = table.Column<DateTime>(nullable: false),
                    PartExpiryDate = table.Column<DateTime>(nullable: false),
                    VehicleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehiclePartRegistration", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehiclePartRegistration_Vehicle_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_BankPayment_BankId",
                table: "BankPayment",
                column: "BankId");

            migrationBuilder.CreateIndex(
                name: "IX_BankPayment_TerminalId",
                table: "BankPayment",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_BookingTypeId",
                table: "Booking",
                column: "BookingTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_PayStackReference",
                table: "Booking",
                column: "PayStackReference");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_PayStackWebhookReference",
                table: "Booking",
                column: "PayStackWebhookReference");

            migrationBuilder.CreateIndex(
                name: "IX_CashRemittant_TerminalId",
                table: "CashRemittant",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_PhoneNumber",
                table: "Customer",
                column: "PhoneNumber",
                unique: true,
                filter: "[PhoneNumber] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_WalletId",
                table: "Customer",
                column: "WalletId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_MaintenanceWalletId",
                table: "Driver",
                column: "MaintenanceWalletId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_WalletId",
                table: "Driver",
                column: "WalletId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_DepartmentId",
                table: "Employee",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_PartnerId",
                table: "Employee",
                column: "PartnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_PositionId",
                table: "Employee",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_TerminalId",
                table: "Employee",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_WalletId",
                table: "Employee",
                column: "WalletId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeRoute_EmployeeId",
                table: "EmployeeRoute",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeRoute_RouteId",
                table: "EmployeeRoute",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeRoute_TerminalId",
                table: "EmployeeRoute",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_Expense_TerminalId",
                table: "Expense",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_Fare_RouteId",
                table: "Fare",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_Fare_VehicleModelId",
                table: "Fare",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_FareCalendar_RouteId",
                table: "FareCalendar",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_HiredBooking_PickupPointId",
                table: "HiredBooking",
                column: "PickupPointId");

            migrationBuilder.CreateIndex(
                name: "IX_HiredBooking_VehicleTripRegistrationId",
                table: "HiredBooking",
                column: "VehicleTripRegistrationId");

            migrationBuilder.CreateIndex(
                name: "IX_JourneyManagement_VehicleTripRegistrationId",
                table: "JourneyManagement",
                column: "VehicleTripRegistrationId");

            migrationBuilder.CreateIndex(
                name: "IX_ManifestManagement_VehicleModelId",
                table: "ManifestManagement",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ManifestManagement_VehicleTripRegistrationId",
                table: "ManifestManagement",
                column: "VehicleTripRegistrationId");

            migrationBuilder.CreateIndex(
                name: "IX_OtherIncome_TerminalId",
                table: "OtherIncome",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_Partner_PartnerApplicationId",
                table: "Partner",
                column: "PartnerApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_Partner_WalletId",
                table: "Partner",
                column: "WalletId");

            migrationBuilder.CreateIndex(
                name: "IX_PartnerApplication_ApproverId",
                table: "PartnerApplication",
                column: "ApproverId");

            migrationBuilder.CreateIndex(
                name: "IX_PartnerApplication_IdentificationTypeId",
                table: "PartnerApplication",
                column: "IdentificationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PartnersWallet_PartnerId",
                table: "PartnersWallet",
                column: "PartnerId");

            migrationBuilder.CreateIndex(
                name: "IX_PartnersWallet_WalletNumber",
                table: "PartnersWallet",
                column: "WalletNumber",
                unique: true,
                filter: "[WalletNumber] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_PartnersWalletTransaction_PartnersWalletId",
                table: "PartnersWalletTransaction",
                column: "PartnersWalletId");

            migrationBuilder.CreateIndex(
                name: "IX_PartnersWalletTransaction_WalletId",
                table: "PartnersWalletTransaction",
                column: "WalletId");

            migrationBuilder.CreateIndex(
                name: "IX_PickupPoint_RouteId",
                table: "PickupPoint",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_PickupPoint_TerminalId",
                table: "PickupPoint",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_PickupPoint_TripId",
                table: "PickupPoint",
                column: "TripId");

            migrationBuilder.CreateIndex(
                name: "IX_Route_DepartureTerminalId",
                table: "Route",
                column: "DepartureTerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_Route_DestinationTerminalId",
                table: "Route",
                column: "DestinationTerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_SeatManagement_BookingId1",
                table: "SeatManagement",
                column: "BookingId1");

            migrationBuilder.CreateIndex(
                name: "IX_SeatManagement_PickUpPointId",
                table: "SeatManagement",
                column: "PickUpPointId");

            migrationBuilder.CreateIndex(
                name: "IX_SeatManagement_RouteId",
                table: "SeatManagement",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_SeatManagement_SubRouteId",
                table: "SeatManagement",
                column: "SubRouteId");

            migrationBuilder.CreateIndex(
                name: "IX_SeatManagement_VehicleModelId",
                table: "SeatManagement",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SeatManagement_VehicleTripRegistrationId",
                table: "SeatManagement",
                column: "VehicleTripRegistrationId");

            migrationBuilder.CreateIndex(
                name: "IX_State_RegionId",
                table: "State",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_Store_TerminalId",
                table: "Store",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_SubRoute_RouteId",
                table: "SubRoute",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_Terminal_StateId",
                table: "Terminal",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_Trip_RouteId",
                table: "Trip",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_Trip_VehicleModelId",
                table: "Trip",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_TripAvailability_TripId",
                table: "TripAvailability",
                column: "TripId");

            migrationBuilder.CreateIndex(
                name: "IX_TripAvailability_TripSettingId",
                table: "TripAvailability",
                column: "TripSettingId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_LocationId",
                table: "Vehicle",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_PartnerId",
                table: "Vehicle",
                column: "PartnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_VehicleModelId",
                table: "Vehicle",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleModel_VehicleMakeId",
                table: "VehicleModel",
                column: "VehicleMakeId");

            migrationBuilder.CreateIndex(
                name: "IX_VehiclePart_VehicleModelId",
                table: "VehiclePart",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_VehiclePartPosition_VehiclePartId",
                table: "VehiclePartPosition",
                column: "VehiclePartId");

            migrationBuilder.CreateIndex(
                name: "IX_VehiclePartRegistration_VehicleId",
                table: "VehiclePartRegistration",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTripRegistration_BookingId",
                table: "VehicleTripRegistration",
                column: "BookingId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTripRegistration_BookingTypeId",
                table: "VehicleTripRegistration",
                column: "BookingTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTripRegistration_TripId",
                table: "VehicleTripRegistration",
                column: "TripId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTripRegistration_VehicleModelId",
                table: "VehicleTripRegistration",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Wallet_WalletNumber",
                table: "Wallet",
                column: "WalletNumber",
                unique: true,
                filter: "[WalletNumber] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_WalletNumber_WalletPan",
                table: "WalletNumber",
                column: "WalletPan",
                unique: true,
                filter: "[WalletPan] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_WalletTransaction_WalletId",
                table: "WalletTransaction",
                column: "WalletId");

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Partner_PartnerId",
                table: "Employee",
                column: "PartnerId",
                principalTable: "Partner",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Terminal_TerminalId",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Wallet_WalletId",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Partner_Wallet_WalletId",
                table: "Partner");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Department_DepartmentId",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Partner_PartnerId",
                table: "Employee");

            migrationBuilder.DropTable(
                name: "AccountSummary");

            migrationBuilder.DropTable(
                name: "AccountTransaction");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "BankPayment");

            migrationBuilder.DropTable(
                name: "CashRemittant");

            migrationBuilder.DropTable(
                name: "Coupon");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "CustomerCouponRegistration");

            migrationBuilder.DropTable(
                name: "Discount");

            migrationBuilder.DropTable(
                name: "Driver");

            migrationBuilder.DropTable(
                name: "DriverAccount");

            migrationBuilder.DropTable(
                name: "EmployeeRoute");

            migrationBuilder.DropTable(
                name: "Expense");

            migrationBuilder.DropTable(
                name: "Fare");

            migrationBuilder.DropTable(
                name: "FareCalendar");

            migrationBuilder.DropTable(
                name: "GeneralLedger");

            migrationBuilder.DropTable(
                name: "HiredBooking");

            migrationBuilder.DropTable(
                name: "JourneyManagement");

            migrationBuilder.DropTable(
                name: "ManifestManagement");

            migrationBuilder.DropTable(
                name: "OtherIncome");

            migrationBuilder.DropTable(
                name: "PartnerEnquiry");

            migrationBuilder.DropTable(
                name: "PartnersWalletTransaction");

            migrationBuilder.DropTable(
                name: "PaymentGatewayStatus");

            migrationBuilder.DropTable(
                name: "SeatManagement");

            migrationBuilder.DropTable(
                name: "Store");

            migrationBuilder.DropTable(
                name: "TripAvailability");

            migrationBuilder.DropTable(
                name: "VehicleMileage");

            migrationBuilder.DropTable(
                name: "VehiclePartPosition");

            migrationBuilder.DropTable(
                name: "VehiclePartRegistration");

            migrationBuilder.DropTable(
                name: "Vendor");

            migrationBuilder.DropTable(
                name: "WalletNumber");

            migrationBuilder.DropTable(
                name: "WalletTransaction");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Bank");

            migrationBuilder.DropTable(
                name: "PartnersWallet");

            migrationBuilder.DropTable(
                name: "PickupPoint");

            migrationBuilder.DropTable(
                name: "SubRoute");

            migrationBuilder.DropTable(
                name: "VehicleTripRegistration");

            migrationBuilder.DropTable(
                name: "TripSetting");

            migrationBuilder.DropTable(
                name: "VehiclePart");

            migrationBuilder.DropTable(
                name: "Vehicle");

            migrationBuilder.DropTable(
                name: "Booking");

            migrationBuilder.DropTable(
                name: "Trip");

            migrationBuilder.DropTable(
                name: "BookingType");

            migrationBuilder.DropTable(
                name: "PayStackPaymentResponse");

            migrationBuilder.DropTable(
                name: "PayStackWebhookResponse");

            migrationBuilder.DropTable(
                name: "Route");

            migrationBuilder.DropTable(
                name: "VehicleModel");

            migrationBuilder.DropTable(
                name: "VehicleMake");

            migrationBuilder.DropTable(
                name: "Terminal");

            migrationBuilder.DropTable(
                name: "State");

            migrationBuilder.DropTable(
                name: "Region");

            migrationBuilder.DropTable(
                name: "Wallet");

            migrationBuilder.DropTable(
                name: "Department");

            migrationBuilder.DropTable(
                name: "Partner");

            migrationBuilder.DropTable(
                name: "PartnerApplication");

            migrationBuilder.DropTable(
                name: "Employee");

            migrationBuilder.DropTable(
                name: "IdentificationType");

            migrationBuilder.DropTable(
                name: "Position");
        }
    }
}
