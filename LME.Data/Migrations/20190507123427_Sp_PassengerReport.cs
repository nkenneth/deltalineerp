﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LME.Data.Migrations
{
    public partial class Sp_PassengerReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sql = @"CREATE PROCEDURE Sp_PassengerReport @Keyword nvarchar(50), @StartDate DateTime, @EndDate DateTime
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select distinct PhoneNumber, FullName, COUNT(PhoneNumber) over (Partition by PhoneNumber, Fullname) NoOfTickets from SeatManagement
where (CreationTime between @StartDate and  @EndDate) and 
							   (@keyword is null or (FullName like '%' + @keyword +'%' or 
													 PhoneNumber like '%' + @keyword +'%' ))
END";

            migrationBuilder.Sql(sql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
